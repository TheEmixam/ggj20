﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollActivation : MonoBehaviour
{

    [SerializeField]
    Animator _animator;

    [SerializeField]
    Rigidbody[] _gamePlayRigidBody;

    [SerializeField]
    Collider[] _gamePlayerCollider;

    [SerializeField]
    Rigidbody[] _ragDollRigidBody;

    [SerializeField]
    Rigidbody[] _ragDollRigidBodyToApplyForce;

    [SerializeField]
    Collider[] _ragDollColliders;

    [SerializeField]
    bool _useSpeedMemory;

    Vector3[] _rbLastPosition;
    Vector3[] _rbNewPosition;
    Vector3[] _rbSpeed;

    Vector3[] _initialPosition;
    Quaternion[] _initialRotation;

    [SerializeField]
    float _forceBase;

    bool _ragDollIsActif;

    private void Awake()
    {
        _initialPosition = new Vector3[_ragDollRigidBody.Length];
        _initialRotation = new Quaternion[_ragDollRigidBody.Length];

        for (int i = 0; i < _ragDollRigidBody.Length; i++)
        {
            _initialPosition[i] = _ragDollRigidBody[i].transform.localPosition;
            _initialRotation[i] = _ragDollRigidBody[i].transform.localRotation;
        }
    }

    private void Start()
    {
        _rbLastPosition = new Vector3[_ragDollRigidBody.Length];
        _rbSpeed = new Vector3[_ragDollRigidBody.Length];
        _rbNewPosition = new Vector3[_ragDollRigidBody.Length];

    }

    private void Update()
    {
        if (!_ragDollIsActif && _useSpeedMemory)
        {
            for (int i = 0; i < _ragDollRigidBody.Length; i++)
            {
                _rbNewPosition[i] = _ragDollRigidBody[i].position;
                _rbSpeed[i] = (_rbNewPosition[i] - _rbLastPosition[i]) / Time.deltaTime;
                _rbLastPosition[i] = _rbNewPosition[i];
            }
        }
    }

    public void SetRagdollActive(bool actif)
    {
        _animator.enabled = !actif;
        _ragDollIsActif = actif;

        for (int i = 0; i < _gamePlayRigidBody.Length; i++)
        {
            _ragDollRigidBody[i].useGravity = !actif;
            _ragDollRigidBody[i].isKinematic = actif;
        }

        for (int i = 0; i < _gamePlayerCollider.Length; i++)
        {
            _ragDollColliders[i].enabled = !actif;
        }

        for (int i = 0; i < _ragDollRigidBody.Length; i++)
        {
            _ragDollRigidBody[i].useGravity = actif;
            _ragDollRigidBody[i].isKinematic = !actif;
        }

        for (int i = 0; i < _ragDollColliders.Length; i++)
        {
            _ragDollColliders[i].enabled = actif;
        }

        if (actif && _useSpeedMemory)
        {
            for (int i = 0; i < _ragDollRigidBody.Length; i++)
            {
                _ragDollRigidBody[i].velocity = _rbSpeed[i];
            }
        }

        if (!actif)
        {
            for (int i = 0; i < _ragDollRigidBody.Length; i++)
            {
                _ragDollRigidBody[i].transform.localPosition = _initialPosition[i];
                _ragDollRigidBody[i].transform.localRotation = _initialRotation[i];
            }
        }
    }

    public void AddForce(Vector3 direction)
    {
        for (int i = 0; i < _ragDollRigidBodyToApplyForce.Length; i++)
        {
            Rigidbody rb = _ragDollRigidBodyToApplyForce[i];
            //Vector3 force = (rb.position - source).normalized * _forceBase;
            rb.AddForce(direction.normalized * _forceBase, ForceMode.Impulse);
        }
    }
}

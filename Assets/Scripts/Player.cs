﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{

    private Vector2 inputMove;
    private Vector2 inputRotation;
    private Vector2 lookDirection;

    public Rigidbody Rigidbody;

    public float SpeedMaxRun;
    public float SpeedMaxWalk;
    public float SpeedMaxThrow;

    public float MaxForce;

    public AnimationCurve DeltaSpeedToForce;

    private float currentRotation;

    private float rotationSpeed;
    private float rotationSpeedVelocity;
    public float RotationSpeedSmooth;

    public Transform anchorPoint1;
    public Transform anchorPoint2;

    public PickUp pickUp1;
    public PickUp pickUp2;

    public float dashDuration;
    private bool isDashing;
    private bool isDashCoolDown;
    public float DashCoolDown;
    public AnimationCurve dashCurve;
    public float maxSpeedDash;
    public float maxDistanceDash;

    public PlayerInput playerInput;

    private bool isStartThrowing;
    public float ThrowMaxDuration;
    private float ThrowStartTime;

    private bool isConsumingItem;

    private bool isUsing;

    public Animator animator;

	[Header("Health")]
    public FloatVariable StartingLife;
    public FloatVariable Life;
	public AnimationCurve RedFlashFrequencyCurve;
	public AnimationCurve RedFlashSpikeCurve;
	public AnimationCurve RedFlashSpikeDuration;
	public SkinnedMeshRenderer[] MeshRenderers;

	[Header("IFrames")]
	public float IFrameDuration;

	[Header("Heal")]
	public float HealAnimDuration;
	public AnimationCurve HealAnimCurve;

	private float _lastTimeHit;

	private Dictionary<string, float> _speedModifiers = new Dictionary<string, float>();
    private bool blockInputMove;

    [Header("Sound")]

    public Demute.AudioEvent deathSound;
    public Demute.AudioEvent hurtSound;
    public Demute.AudioEvent dashSound;
    public Demute.AudioEvent throwStart;
    public Demute.AudioEvent throwStop;

    [Header("-- EFFECTS --")]
    public GameObject ParticlesGO;
    public ParticleSystem[] Particles;
    public float _delayDamageFire = 1.5f;
    public float _delayUnsetOnFire = 5.0f;

    private Coroutine _particleCoroutine = null;
    private float _fireBuildUp = 0.0f;
    private float _unSetBuildUp = 0.0f;
    private float _fireDamage = 0.0f;

    private bool _onFire = false;
    public Demute.AudioEvent healSound;
    
    public float CurrentSpeedModifier
	{
		get
		{
			float m = 1.0f;
			List<float> modifiers = new List<float>(_speedModifiers.Values);
			for(int i = 0; i < modifiers.Count; i++)
			{
				m *= modifiers[i];
			}
			return m;
		}
	}

	public void SetSpeedModifier(string key, float value)
	{
		if (_speedModifiers.ContainsKey(key))
			_speedModifiers[key] = value;
		else
			_speedModifiers.Add(key, value);
	}
	public void RemoveSpeedModifier(string key)
	{
		if (_speedModifiers.ContainsKey(key))
			_speedModifiers.Remove(key);
	}

    public bool IsDead
    {
        get
        {
            return Life.Value <= 0;
        }
    }

    private void Awake()
    {
        _speedModifiers = new Dictionary<string, float>();
        Rigidbody = GetComponent<Rigidbody>();
        isDashCoolDown = false;
        isStartThrowing = false;
        Life.Value = StartingLife.Value;
    }


    // Start is called before the first frame update
    void Start()
    {
        GetComponent<RagdollActivation>().SetRagdollActive(false);
        currentRotation = transform.localEulerAngles.y;
        StartCoroutine(HealthFlashCoroutine());
    }

	IEnumerator HealthFlashCoroutine()
	{
		float healthT;
		float startTime;
		while (!IsDead)
		{       
			//yield return new WaitUntil(() => RedFlashFrequencyCurve.Evaluate(healthT) > 0.0f);
			startTime = Time.time;
			while (RedFlashFrequencyCurve.Evaluate(Life.Value / StartingLife.Value) == 0.0f || Time.time - startTime < 1.0f / RedFlashFrequencyCurve.Evaluate(Life.Value / StartingLife.Value))
				yield return null;
			//float sValue = Mathf.Sin(Time.time * RedFlashFrequencyCurve.Evaluate(Life.Value / StartingLife.Value));

			healthT = Life.Value / StartingLife.Value;
			startTime = Time.time;
			while (Time.time - startTime < RedFlashSpikeDuration.Evaluate(healthT))
			{
				float t = (Time.time - startTime) / RedFlashSpikeDuration.Evaluate(healthT);

				for (int i = 0; i < MeshRenderers.Length; i++)
				{
					MeshRenderers[i].material.SetFloat("DamageSlider", RedFlashSpikeCurve.Evaluate(t));
				}

				yield return null;
			}
			for (int i = 0; i < MeshRenderers.Length; i++)
			{
				MeshRenderers[i].material.SetFloat("DamageSlider", 0.0f);
			}
		}
		for (int i = 0; i < MeshRenderers.Length; i++)
		{
			MeshRenderers[i].material.SetFloat("DamageSlider", 0.0f);
			MeshRenderers[i].material.SetColor("_EmissionColor", Color.black);
		}
	}

    public bool IsInvicible()
    {
        return isDashing || Time.time - _lastTimeHit < IFrameDuration;
    }

    public void TakeDamage(float damage, Vector3 direction)
    {
        if (IsInvicible())
            return;

		_lastTimeHit = Time.time;

		Life.Value = Life.Value - damage;
        if (Life.Value <= 0)
            Death(direction);

        animator.SetTrigger("TakeDamage");
        hurtSound.Post(gameObject);
    }

    private void Death(Vector3 source)
    {
        StartCoroutine(UnSetOnFire());
        deathSound.Post(gameObject);
        Debug.Log("Player Is Death");
        GetComponent<RagdollActivation>().SetRagdollActive(true);
        GetComponent<RagdollActivation>().AddForce(source);
    }

    public void Victory()
    {
        animator.SetBool("Win", true);
        blockInputMove = true;
    }

    // Update is called once per frame
    void Update()
    {

        if(GameSingleton.Instance && GameSingleton.Instance.GameFinished)
        {
            blockInputMove = true;
        }

        if (blockInputMove)
        {
            inputMove = new Vector2(0,0);
            inputRotation = Vector2.down;
        }

        if(_onFire)
        {
            _fireBuildUp += Time.deltaTime;
            _unSetBuildUp += Time.deltaTime;

            if(_fireBuildUp >= _delayDamageFire)
            {
                _fireBuildUp = 0.0f;
                TakeDamage(_fireDamage, Vector3.zero);
            }

            if(_unSetBuildUp >= _delayUnsetOnFire)
            {
                _unSetBuildUp = 0.0f;
                StartCoroutine(UnSetOnFire());
            }
        }
        else
        {
            _fireBuildUp = 0.0f;
            _unSetBuildUp = 0.0f;
        }

        float currentSpeed = 0;
        Vector3 velocity3 = Rigidbody.velocity;
        Vector2 velocity = new Vector2(velocity3.x, velocity3.z);
        currentSpeed = velocity.magnitude;

        bool islooking = inputRotation.magnitude > 0.5f;
        float SpeedMax = islooking ? SpeedMaxWalk : SpeedMaxRun;
        if (isStartThrowing)
            SpeedMax = SpeedMaxThrow;

		//Apply speed modifiers
		SpeedMax *= CurrentSpeedModifier;

        float targetVelocity = inputMove.magnitude * SpeedMax;
        float deltaVelocity = (targetVelocity - currentSpeed) / SpeedMax;
        float forceIntencity = DeltaSpeedToForce.Evaluate(deltaVelocity) * MaxForce;

        Vector2 force = inputMove.normalized * forceIntencity;
        Vector3 force3 = new Vector3(force.x, 0, force.y);

        if (!isDashing)
            Rigidbody.AddForce(force3, ForceMode.Force);

        lookDirection = islooking ? inputRotation : inputMove;
        float lookRotation = 0;
        if (lookDirection.magnitude > 0.1f)
            lookRotation = Mathf.Atan2(lookDirection.x, lookDirection.y) * Mathf.Rad2Deg;
        else
            lookRotation = currentRotation;

        currentRotation = Mathf.SmoothDampAngle(currentRotation, lookRotation, ref rotationSpeedVelocity, RotationSpeedSmooth);
        Rigidbody.rotation = Quaternion.Euler(0, currentRotation, 0);

        float forwardSpeed = Vector3.Dot(Rigidbody.velocity, transform.forward);
        float rightSpeed = Vector3.Dot(Rigidbody.velocity, transform.right);

        animator.SetFloat("Forward", forwardSpeed);
        animator.SetFloat("Right", rightSpeed);

		for (int i = 0; i < MeshRenderers.Length; i++)
		{
			MeshRenderers[i].material.SetFloat("InvulnerableSlider", IsInvicible() ? 1.0f : 0.0f);
		}

		if (Input.GetKeyDown(KeyCode.P))
        {
            Death(Vector3.up);
        }

        animator.SetFloat("Sin", Mathf.Sin(Time.time*4)/2+0.5f);
    }

    public bool IsStartThrowing()
    {
        return isStartThrowing;
    }

    public float GetThrowPower()
    {
        return (Time.time - ThrowStartTime) / ThrowMaxDuration;
    }

    public IEnumerator Dash()
    {
        dashSound.Post(gameObject);
        Vector2 dashDirection = inputMove.normalized;
        float startTime = Time.time;
        isDashing = true;
        isDashCoolDown = true;
        float normTime = 0;
        Vector3 startPoint = transform.position;
        Vector3 endPoint = transform.position + new Vector3(dashDirection.x, 0, dashDirection.y) * maxDistanceDash;

        Vector3 lastTargetPosition = startPoint;
        while (normTime < 1)
        {
            normTime = (Time.time - startTime) / dashDuration;
            float dashCurrentSpeed = dashCurve.Evaluate(normTime) * maxSpeedDash;
            Vector2 dashSpeed = dashDirection * dashCurrentSpeed;
            //rigidbody.velocity = new Vector3(dashSpeed.x, 0, dashSpeed.y);
            //transform.position = Vector3.Lerp(startPoint, endPoint, dashCurve.Evaluate(normTime));
            Vector3 targetPosition = Vector3.Lerp(startPoint, endPoint, dashCurve.Evaluate(normTime));
            Rigidbody.velocity = (targetPosition - lastTargetPosition)/Time.fixedDeltaTime;
            lastTargetPosition = targetPosition;
            yield return new WaitForFixedUpdate();
        }
        isDashing = false;
        yield return new WaitForSeconds(DashCoolDown);
        isDashCoolDown = false;
    }

    public void Equip(PickUp pickUp)
    {
        if (pickUp1 == null)
        {
            pickUp1 = pickUp;
            pickUp.transform.SetParent(anchorPoint1);
        }
        else if (pickUp2 == null)
        {
            pickUp2 = pickUp;
            pickUp.transform.SetParent(anchorPoint2);
        }
        else
        {
            Debug.LogError("Oups");
        }
        pickUp.transform.localPosition = Vector3.zero;
        pickUp.transform.localRotation = Quaternion.identity;

        pickUp.Item.VisualItem.pickupSound.Post(gameObject);
    }

    public bool HasEmptySlotItem()
    {
        return pickUp2 == null;
    }

    public void SetMove(InputAction.CallbackContext input)
    {
        inputMove = input.ReadValue<Vector2>();
    }

    public void SetRotation(InputAction.CallbackContext input)
    {
        inputRotation = input.ReadValue<Vector2>();
    }

    public void PressThrow(InputAction.CallbackContext input)
    {
        if (input.phase == InputActionPhase.Started && pickUp1)
        {
            if (input.ReadValue<float>() > 0.5f && !isStartThrowing && !isConsumingItem)
            {
                isStartThrowing = true;
                ThrowStartTime = Time.time;
                animator.SetBool("Throw",true);
                isConsumingItem = true;
                throwStart.Post(gameObject);

            }
            else if (input.ReadValue<float>() < 0.5f && isStartThrowing)
            {
                animator.SetBool("Throw", false);
            }

        }
    }

    public void Throw()
    {
        if (isStartThrowing && isConsumingItem)
        {
            throwStop.Post(gameObject);
            float throwPower = (Time.time - ThrowStartTime) / ThrowMaxDuration;
            throwPower = Mathf.Clamp01(throwPower);
            pickUp1.Launch(this, this.transform.forward, throwPower);
            LoseItem();
            isStartThrowing = false;
            isConsumingItem = false;
        }
    }

    public void PressUse(InputAction.CallbackContext input)
    {
        if (input.phase == InputActionPhase.Started && input.ReadValue<float>() > 0.5f && !isConsumingItem)
        {
            if (pickUp1)
            {
                isUsing = true;
                animator.SetTrigger("Use");
                isConsumingItem = true;
            }
        }
    }

    public void Use()
    {
        if (isUsing)
        {
			if(Life.Value != StartingLife.Value)
				StartCoroutine(HealCoroutine());
            Life.Value = Mathf.Min(Life.Value + pickUp1.Item.VisualItem.LifeRegen, StartingLife.Value);
            pickUp1.Use(this);
            LoseItem();
            //animator.SetTrigger("Use");
            isUsing = false;
            isConsumingItem = false;
        }
    }

	IEnumerator HealCoroutine()
	{
        AudioManager.PostEvent(healSound, gameObject);

        float start = Time.time;
		while(Time.time - start < HealAnimDuration)
		{
			for (int i = 0; i < MeshRenderers.Length; i++)
			{
				MeshRenderers[i].material.SetFloat("HealSlider", HealAnimCurve.Evaluate((Time.time - start) / HealAnimDuration));
			}
			yield return null;
		}

		for (int i = 0; i < MeshRenderers.Length; i++)
		{
			MeshRenderers[i].material.SetFloat("HealSlider", 0.0f);
		}
	}

    public void LoseItem()
    {
        pickUp1.transform.SetParent(null);
        pickUp1 = null;
        if (pickUp2)
        {
            pickUp1 = pickUp2;
            pickUp2 = null;
            pickUp1.transform.SetParent(anchorPoint1);
        }

    }

    public void PressDash(InputAction.CallbackContext input)
    {
        if (input.phase == InputActionPhase.Started && input.ReadValue<float>() > 0.5f)
            if (!isDashCoolDown)
                StartCoroutine(Dash());
    }

    public void PressExchange(InputAction.CallbackContext input)
    {
        if (input.phase == InputActionPhase.Started && input.ReadValue<float>() > 0.5f && !isConsumingItem)
        {
            if (pickUp1 != null && pickUp2 != null)
            {
                PickUp temp = pickUp1;
                pickUp1 = pickUp2;
                pickUp2 = temp;
                pickUp1.transform.SetParent(anchorPoint1);
                pickUp2.transform.SetParent(anchorPoint2);
                pickUp1.transform.localPosition = Vector3.zero;
                pickUp2.transform.localPosition = Vector3.zero;
                pickUp1.transform.localRotation = Quaternion.identity;
                pickUp2.transform.localRotation = Quaternion.identity;
            }

        }
    }

    public void SetOnFire(float damage)
    {
        _onFire = true;
        _fireDamage = damage;
        SetSpeedModifier("fire", 1.5f);
        ParticlesGO.SetActive(true);

        foreach (ParticleSystem particle in Particles)
        {
            particle.Play();
        }
    }

    public IEnumerator UnSetOnFire()
    {
        foreach (ParticleSystem particle in Particles)
        {
            particle.Stop();
        }

        yield return new WaitForSeconds(0.5f);

        _onFire = false;
        _fireDamage = 0.0f;
        RemoveSpeedModifier("fire");
        ParticlesGO.SetActive(false);
        _particleCoroutine = null;
    }

    private void OnCollisionEnter(Collision collision)
    {
        Player player = collision.transform.GetComponent<Player>();

        if(player && _onFire)
        {
            player.SetOnFire(0.5f);
        }
    }
}

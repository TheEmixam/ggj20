﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SpawnZone : MonoBehaviour
{
    [SerializeField]
    private float _minX = 0.0f;
    [SerializeField]
    private float _maxX = 0.0f;
    [SerializeField]
    private float _minY = 0.0f;
    [SerializeField]
    private float _maxY = 0.0f;

    public Vector3 GetRandomPoint()
    {
        float randX = Random.Range(_minX, _maxX);
        float randY = Random.Range(_minY, _maxY);
        Vector3 randomPos = new Vector3(transform.position.x + randX, transform.position.y, transform.position.z + randY);

        if(NavMesh.SamplePosition(randomPos, out NavMeshHit hit, 10.0f, NavMesh.AllAreas))
        {
            return hit.position;
        }

        return transform.position;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(new Vector3(_minX, transform.position.y, _minY), new Vector3(_minX, transform.position.y, _maxY));
        Gizmos.DrawLine(new Vector3(_minX, transform.position.y, _maxY), new Vector3(_maxX, transform.position.y, _maxY));
        Gizmos.DrawLine(new Vector3(_maxX, transform.position.y, _maxY), new Vector3(_maxX, transform.position.y, _minY));
        Gizmos.DrawLine(new Vector3(_maxX, transform.position.y, _minY), new Vector3(_minX, transform.position.y, _minY));

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(new Vector3(_minX, transform.position.y, _minY), 0.5f);
        Gizmos.DrawWireSphere(new Vector3(_minX, transform.position.y, _maxY), 0.5f);
        Gizmos.DrawWireSphere(new Vector3(_maxX, transform.position.y, _maxY), 0.5f);
        Gizmos.DrawWireSphere(new Vector3(_maxX, transform.position.y, _minY), 0.5f);
    }
#endif
}

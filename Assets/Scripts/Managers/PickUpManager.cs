﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpManager : MonoBehaviour
{
    [Header("-- ITEMS --")]
    [SerializeField]
    private VisualItem[] _visuals;
    [SerializeField]
    private EffectItem[] _effects;

    [Header("-- SPAWN --")]
    [SerializeField]
    private SpawnZone _spawn;
    [SerializeField]
    private float _spawnHeight = 5.0f;
    [SerializeField]
    private float _timeSpawn = 3.0f;
    [SerializeField]
    private float _spawnDelay = 2.0f;
    [SerializeField]
    private int _nbItemMax = 20;

    [Header("-- PREFAB --")]
    [SerializeField]
    private PickUp _pickupPrefab;

    private Item[] _items;
    private Transform _transform;
    private float _currentSpawnTime = 0.0f;
    private List<PickUp> _pickups = new List<PickUp>();

    private void Awake()
    {
        Init();
        _currentSpawnTime = _spawnDelay;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E)) SpawnItem();

        _currentSpawnTime += Time.deltaTime;

        if(_currentSpawnTime >= _spawnDelay && _pickups.Count < _nbItemMax)
        {
            _currentSpawnTime = 0.0f;
            SpawnItem();
        }
    }

    public void Init()
    {
        _transform = transform;
        _items = new Item[_visuals.Length];

        _visuals.Shuffle();
        _effects.Shuffle();

        for(int i = 0; i < _items.Length; i++)
        {
            _items[i] = new Item(_visuals[i], _effects[i]);
        }
    }

    [ContextMenu("Spawn Item")]
    public void SpawnItem()
    {
        if(_items.Length == 0)
        {
            Init();
        }

        Vector3 rand = _spawn.GetRandomPoint();
        PickUp pickup = Instantiate(_pickupPrefab, rand + Vector3.up * _spawnHeight, Quaternion.identity, _transform);
        pickup.Item = _items[Random.Range(0, _items.Length)];
        _pickups.Add(pickup);
        StartCoroutine(SpawnCoroutine(pickup.transform.position, rand + Vector3.up * 0.5f, pickup));
    }

    private IEnumerator SpawnCoroutine(Vector3 start, Vector3 end, PickUp pickup)
    {
        pickup.DisablePickUp();
        Transform t = pickup.transform;
        float elapsedTime = 0.0f;

        while(elapsedTime < _timeSpawn)
        {
            t.transform.position = Vector3.Lerp(start, end, elapsedTime / _timeSpawn);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        t.transform.position = end;
        pickup.EnablePickUp();
    }

    public void RemovePickup(PickUp pickup)
    {
        if(_pickups.Contains(pickup))
        {
            _pickups.Remove(pickup);
        }
    }
}

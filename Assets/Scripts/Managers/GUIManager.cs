﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour
{
    [SerializeField]
    private float _timeFade = 0.5f;
    [SerializeField]
    private Animator _textAnim;
    [SerializeField]
    private Text _text;

    private CanvasGroup _fade;

    private void Awake()
    {
        _fade = GetComponentInChildren<CanvasGroup>();
    }

    public void ShowText(string str)
    {
        _text.text = str;
        _textAnim.enabled = true;
    }

    public IEnumerator FadeOut()
    {
        yield return Fade(1.0f, 0.0f);
    }

    public IEnumerator FadeIn()
    {
        yield return Fade(0.0f, 1.0f);
    }

    private IEnumerator Fade(float start, float end)
    {
        float elapsedTime = 0.0f;

        while (elapsedTime < _timeFade)
        {
            _fade.alpha = Mathf.Lerp(start, end, elapsedTime / _timeFade);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        _fade.alpha = end;
    }
}

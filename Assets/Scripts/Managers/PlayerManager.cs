﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerManager : MonoBehaviour
{
    [SerializeField]
    private Player[] _players;
    [SerializeField]
    private Transform[] _spawns;
    public float DeathDelay = 0.5f;

    private int _nbPlayers = 0;
    private List<Player> _activePlayers = new List<Player>();

    private void Update()
    {
        Player[] players = _activePlayers.ToArray();

        foreach(Player player in players)
        {
            if(player.IsDead && _activePlayers.Contains(player))
            {
                _activePlayers.Remove(player);
            }
        }
    }

    public void SpawnPlayers()
    {
        _players.Shuffle();
        _nbPlayers = Gamepad.all.Count;

        if (_nbPlayers < 2)
        {
            _nbPlayers = 2;
        }
        else if (_nbPlayers > 4)
        {
            _nbPlayers = 4;
        }

        for (int i = 0; i < _nbPlayers; i++)
        {
            _players[i].transform.position = _spawns[i].position;
            _players[i].gameObject.SetActive(true);
            _activePlayers.Add(_players[i]);
        }
    }

    public void ActivatePlayers()
    {
        for (int i = 0; i < _nbPlayers; i++)
        {
            _players[i].playerInput.enabled = true;
        }
    }

    public List<Player> GetActivePlayers()
    {
        return _activePlayers;
    }
}

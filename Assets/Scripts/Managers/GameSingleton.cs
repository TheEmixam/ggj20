﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSingleton : MonoBehaviour
{
    public static GameSingleton Instance;

    [SerializeField]
    private PickUpManager _pickupManager;
    [SerializeField]
    private PlayerManager _playerManager;
    [SerializeField]
    private GUIManager _guiManager;

    public bool GameFinished
    {
        get
        {
            return _playerManager.GetActivePlayers().Count <= 1;
        }
    }

    public bool OneLeft
    {
        get
        {
            return _playerManager.GetActivePlayers().Count == 1;
        }
    }

    public bool HaveWinner { get; private set; } = false;

    private void Awake()
    {
        Instance = this;
    }

	private void Start()
	{
		StartCoroutine(GameLoop());
	}

	private IEnumerator GameLoop()
    {
        _playerManager.SpawnPlayers();

        yield return _guiManager.FadeOut();

        _pickupManager.gameObject.SetActive(true);
        _playerManager.ActivatePlayers();

        yield return new WaitUntil(() => GameFinished);
        
        if(OneLeft)
        {
            yield return new WaitForSeconds(GetDeathDelay());

            if(OneLeft)
            {
                HaveWinner = true;
            }
        }

        string str = HaveWinner ? "WINNER IS " + _playerManager.GetActivePlayers()[0].gameObject.name + " !" : "DRAW !";
        float timeWait = HaveWinner ? 5.0f : 2.0f;

        if(HaveWinner)
        {
            _playerManager.GetActivePlayers()[0].Victory();
        }

        yield return new WaitForSeconds(timeWait);

        _guiManager.ShowText(str);

        yield return new WaitForSeconds(3.0f);

        yield return _guiManager.FadeIn();

		Debug.Log("Reload scene");

		Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name, LoadSceneMode.Single);
    }

    public void RemovePickup(PickUp pickup)
    {
        _pickupManager.RemovePickup(pickup);
    }

    public float GetDeathDelay()
    {
        return _playerManager.DeathDelay;
    }
}

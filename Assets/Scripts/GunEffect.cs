﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunEffect : MonoBehaviour
{

    public GunScriptable _gunScriptable;

    public Player Launcher = null;
    public float speedBase;

    private void Start()
    {
        GetComponent<Rigidbody>().velocity = transform.forward * speedBase;
    }

    public void SetLauncher(Player Launcher)
    {
        this.Launcher = Launcher;
        Physics.IgnoreCollision(Launcher.GetComponent<Collider>(), GetComponent<Collider>(), true);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Player player = collision.gameObject.GetComponent<Player>();
        if (player)
        {
            player.TakeDamage(_gunScriptable.Damage, transform.forward);
        }
        Destroy(gameObject);
    }

}

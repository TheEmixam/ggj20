﻿using System.Collections;
using System.Collections.Generic;
using System;

public static class GameplayTools
{
    private static Random rand = new Random();

    public static void Shuffle<T>(this List<T> list)
    {
        int n = list.Count;

        while (n > 1)
        {
            n--;
            int k = rand.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    public static void Shuffle<T>(this T[] array)
    {
        int n = array.Length;

        while (n > 1)
        {
            n--;
            int k = rand.Next(n + 1);
            T value = array[k];
            array[k] = array[n];
            array[n] = value;
        }
    }
}

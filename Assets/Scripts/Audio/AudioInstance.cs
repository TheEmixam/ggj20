﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Demute
{
    public class AudioInstance
    {
        public bool isPlaying;
        public AudioContainer baseAudioContainer;

        private AudioComponent _audioComponent;
        private int _lastIndex;
        private List<AudioSource> activeAudioSources;
        private float timeSinceLastTrigger;
        private InheritableParameters inheritedValues;
        private bool stopped;

        public void ActivateAudioInstance (AudioComponent audioComponent, AudioContainer audioContainer, int lastIndex, InheritableParameters inheritedValues)
        {
            _audioComponent = audioComponent;
            baseAudioContainer = audioContainer;
            _lastIndex = lastIndex;
            activeAudioSources = new List<AudioSource>();
            isPlaying = true;
            timeSinceLastTrigger = 0;
            this.inheritedValues = inheritedValues;
            stopped = false;

            PlayContainer();
        }

        /// <summary>
        /// Plays a container on this audio component.
        /// </summary>
        /// <param name="audioContainer">Audio container to play.</param>
        private void PlayContainer()
        {
            if (baseAudioContainer.audioObjects.Length <= 0)
            {
                AudioLogger.LogWarningFormat("Tried playing container {0} without an audioclip list", baseAudioContainer.name);
                return;
            }

            switch (baseAudioContainer.containerType)
            {
                case AudioContainer.AudioContainerTypes.Single:
                    _lastIndex = 0;
                    PlaySingleObject();
                    break;
                case AudioContainer.AudioContainerTypes.Random:
                    int newIndex = Random.Range(0, baseAudioContainer.audioObjects.Length);
                    if(_lastIndex == newIndex)
                    {
                        _lastIndex = (newIndex + 1) % baseAudioContainer.audioObjects.Length;
                    }
                    else
                    {
                        _lastIndex = newIndex;
                    }
                    PlaySingleObject();
                    break;
                case AudioContainer.AudioContainerTypes.Sequence:
                    _lastIndex = (_lastIndex + 1) % baseAudioContainer.audioObjects.Length;
                    PlaySingleObject();
                    break;
                case AudioContainer.AudioContainerTypes.Blend:
                    PlayBlendAudio();
                    break;
                default:
                    break;
            }
        }


        /// <summary>
        /// Plays a single audio clip on the game object.
        /// </summary>
        /// <param name="audioContainer">Audio container that holds the audio clip.</param>
        /// <param name="index">Index of the audio clip.</param>
        private void PlaySingleObject()
        {
            if (baseAudioContainer.audioObjects[_lastIndex] == null)
            {
                AudioLogger.LogWarningFormat("Audio container {0} has no audioclip assigned to it at index {1}", baseAudioContainer.name, _lastIndex);
                return;
            }

            InheritableParameters summedValues = AudioObject.CalculateSummedValues(inheritedValues, baseAudioContainer);

            if (baseAudioContainer.audioObjects[_lastIndex].GetType() == typeof(AudioVoice))
            {
                AudioVoice audioVoice = baseAudioContainer.audioObjects[_lastIndex] as AudioVoice;

                InheritableParameters finalValues = AudioObject.CalculateSummedValues(summedValues, audioVoice);

                AudioSource audioSource = _audioComponent.PoolAudioSource(baseAudioContainer.holdEmitterPosition);
                audioSource.name = string.Format("{0} - {1} audio source", _audioComponent.gameObject.name, baseAudioContainer);
                audioSource.transform.position = _audioComponent.transform.position;
                activeAudioSources.Add(audioSource);
                audioSource.clip = audioVoice.audioClip;
                audioSource.loop = audioVoice.loop;
                audioSource.volume = finalValues.volume.value;
                audioSource.pitch = 1 * Mathf.Pow(2, finalValues.pitch.value/1200);
                audioSource.Play();
            }
            if(baseAudioContainer.audioObjects[_lastIndex].GetType() == typeof(AudioContainer))
            {
                AudioContainer childAudioContainer = baseAudioContainer.audioObjects[_lastIndex] as AudioContainer;

                if (!_audioComponent.lastPlayedIndexes.ContainsKey(childAudioContainer))
                {
                    _audioComponent.lastPlayedIndexes.Add(childAudioContainer, 0);
                }
                AudioInstance childAudioInstance = AudioManager.Instance.PoolAudioInstance();
                childAudioInstance.ActivateAudioInstance(_audioComponent, childAudioContainer, _audioComponent.lastPlayedIndexes[childAudioContainer], summedValues);
                _audioComponent.activeInstances.Add(childAudioInstance);
                
            }
        }

        /// <summary>
        /// NOT IMPLEMENTED YET.
        /// </summary>
        private void PlayBlendAudio()
        {
            AudioLogger.LogWarningFormat("Blend containers not implemented yet");
        }

        public void UpdateInstance(float deltaTime)
        {
            bool sourcesPlaying = false;
            for (int i = activeAudioSources.Count-1; i >= 0; i--)
            {
                AudioSource activeAudioSource = activeAudioSources[i];
                if(activeAudioSource.time >= activeAudioSource.clip.length || (activeAudioSource.time == 0 && !activeAudioSource.isPlaying))
                {
                    activeAudioSources.RemoveAt(i);
                    activeAudioSource.Stop();

#if UNITY_EDITOR
                    if (!Application.isPlaying)
                    {
                        Object.DestroyImmediate(activeAudioSource.gameObject);
                        return;
                    }
#endif
                    AudioManager.Instance.pooledAudioSources.Add(activeAudioSource);
                    activeAudioSource.transform.SetParent(AudioManager.Instance.audioPool);
                    activeAudioSource.name = "Inactive audio source";


                }
                else
                {
                    sourcesPlaying = true;
                }
            }

            if (baseAudioContainer.repeat)
            {
                isPlaying = true;

                switch (baseAudioContainer.repeatType)
                {
                    case AudioContainer.AudioRepeatType.WaitForEnd:
                        if(!sourcesPlaying)
                        {
                            timeSinceLastTrigger += deltaTime;
                        }
                        break;
                    case AudioContainer.AudioRepeatType.TriggerDelay:
                        timeSinceLastTrigger += deltaTime;
                        break;
                    default:
                        break;
                }

                if(timeSinceLastTrigger > baseAudioContainer.containerRepeatDelay)
                {
                    timeSinceLastTrigger = 0;
                    PlayContainer();
                }
            }
            else
            {
                isPlaying = sourcesPlaying;
            }


            if (!isPlaying || stopped)
            {
                _audioComponent.activeInstances.Remove(this);
                _audioComponent.lastPlayedIndexes[baseAudioContainer] = _lastIndex;
                AudioManager.Instance.pooledAudioInstances.Add(this);
            }
        }

        /// <summary>
        /// Stops all the instances of an audio container currently playing on the gameobject.
        /// </summary>
        /// <param name="audioContainerToStop"></param>
        public void Stop()
        {
            for (int i = 0; i < activeAudioSources.Count; i++)
            {
                activeAudioSources[i].Stop();

#if UNITY_EDITOR
                if (!Application.isPlaying)
                {
                    Object.DestroyImmediate(activeAudioSources[i].gameObject);
                    continue;
                }
#endif

                AudioManager.Instance.pooledAudioSources.Add(activeAudioSources[i]);
                activeAudioSources[i].transform.SetParent(AudioManager.Instance.audioPool);
            }
            activeAudioSources = new List<AudioSource>();
            isPlaying = false;
            stopped = true;
            for (int i = 0; i < baseAudioContainer.audioObjects.Length; i++)
            {
                for (int j = 0; j < _audioComponent.activeInstances.Count; j++)
                {
                    if(_audioComponent.activeInstances[j].baseAudioContainer == baseAudioContainer.audioObjects[i])
                    {
                        _audioComponent.activeInstances[j].Stop();
                    }
                }
            }
        }
    }
}


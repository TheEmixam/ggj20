﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Demute
{
    /// <summary>
    /// Class managing the audio sources on an object.
    /// </summary>
    public class AudioComponent : MonoBehaviour
    {
        /// <summary>
        /// The audio instances active on this audio component.
        /// </summary>
        public List<AudioInstance> activeInstances = new List<AudioInstance>();

        /// <summary>
        /// The indexes last played for every audio container played on this object.
        /// </summary>
        public Dictionary<AudioContainer, int> lastPlayedIndexes = new Dictionary<AudioContainer, int>();

        /// <summary>
        /// Posts an audio event on this audio component.
        /// </summary>
        /// <param name="audioEvent">The audio event to post.</param>
        public void Post(AudioEvent audioEvent)
        {
            for (int i = 0; i < audioEvent.audioActions.Length; i++)
            {
                AudioEvent.AudioAction audioAction = audioEvent.audioActions[i];

                switch (audioAction.containerAction)
                {
                    case AudioEvent.ContainerAction.Play:
                        if (audioAction.audioContainer == null)
                        {
                            AudioLogger.LogWarningFormat("Audio action {0} of {1} wasn't assigned a container.", i, audioEvent.name);
                        }
                        else
                        {
                            if (!lastPlayedIndexes.ContainsKey(audioAction.audioContainer))
                            {
                                lastPlayedIndexes.Add(audioAction.audioContainer, 0);
                            }
                            AudioInstance audioInstance = AudioManager.Instance.PoolAudioInstance();
                            activeInstances.Add(audioInstance);
                            audioInstance.ActivateAudioInstance(this, audioAction.audioContainer, lastPlayedIndexes[audioAction.audioContainer], InheritableParameters.DEFAULT);
                            
                        }
                        break;
                    case AudioEvent.ContainerAction.Stop:
                        if (audioAction.audioContainer == null)
                        {
                            AudioLogger.LogWarningFormat("Audio action {0} of {1} wasn't assigned a container.", i, audioEvent.name);
                        }
                        else
                        {
                            for (int j = 0; j < activeInstances.Count; j++)
                            {
                                if(activeInstances[j].baseAudioContainer == audioAction.audioContainer)
                                {
                                    activeInstances[j].Stop();
                                }
                            }
                        }
                        break;
                    case AudioEvent.ContainerAction.StopAll:
                        for (int j = 0; j < activeInstances.Count; j++)
                        {
                            activeInstances[j].Stop();
                        }
                        break;
                    case AudioEvent.ContainerAction.PostEvent:
                        if (audioAction.audioContainer == null)
                        {
                            AudioLogger.LogWarningFormat("Audio action {0} of {1} wasn't assigned an event.", i, audioEvent.name);
                        }
                        else
                        {
                            Post(audioAction.audioEvent);
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void LateUpdate()
        {
            for (int i = 0; i < activeInstances.Count; i++)
            {
                activeInstances[i].UpdateInstance(Time.deltaTime);
            }
        }

        public AudioSource PoolAudioSource(bool holdPosition)
        {
            AudioSource audioSource;
            if(AudioManager.Instance.pooledAudioSources.Count > 0)
            {
                audioSource = AudioManager.Instance.pooledAudioSources[0];
                AudioManager.Instance.pooledAudioSources.Remove(audioSource);
            }
            else
            {
                audioSource = new GameObject(gameObject.name + " audiosource").AddComponent<AudioSource>();
#if UNITY_EDITOR

                if (!Application.isPlaying)
                {
                    audioSource.gameObject.hideFlags = HideFlags.HideAndDontSave;
                }

#endif
            }

            if (holdPosition)
            {
                audioSource.transform.position = transform.position;
            }
            else
            {
                audioSource.transform.SetParent(transform);
            }

            return audioSource;
        }

#if UNITY_EDITOR

        private double lastUpdateTime = 0;

        public void RegisterEditorAudio()
        {
            UnityEditor.EditorApplication.update += EditorUpdate;
        }

        public void UnregisterEditorAudio()
        {
            UnityEditor.EditorApplication.update -= EditorUpdate;
        }

        public void EditorUpdate()
        {
            if (Application.isPlaying)
            {
                UnregisterEditorAudio();
                return;
            }

            if(lastUpdateTime == 0)
            {
                lastUpdateTime = UnityEditor.EditorApplication.timeSinceStartup;
            }

            float deltaTime = (float) (UnityEditor.EditorApplication.timeSinceStartup - lastUpdateTime);

            lastUpdateTime = UnityEditor.EditorApplication.timeSinceStartup;

            for (int i = 0; i < activeInstances.Count; i++)
            {
                activeInstances[i].UpdateInstance(deltaTime);
            }

            if(activeInstances.Count <= 0)
            {
                UnregisterEditorAudio();
                DestroyImmediate(gameObject);
            }
        }
#endif

    }
}



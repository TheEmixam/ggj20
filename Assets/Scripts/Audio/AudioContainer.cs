﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Demute
{
    /// <summary>
    /// Scriptable object defining an audio container.
    /// </summary>
    [CreateAssetMenu(fileName = "New Audio Container", menuName = "Audio/Audio Container")]
    public class AudioContainer : AudioObject
    {
        /// <summary>
        /// Types of possible audio containers.
        /// </summary>
        public enum AudioContainerTypes { Single, Random, Sequence, Blend };

        /// <summary>
        /// The type of container this is (defines play method).
        /// </summary>
        [Tooltip("The type of container this is (defines play method).")]
        public AudioContainerTypes containerType;

        /// <summary>
        /// The containers or voices this container holds.
        /// </summary>
        [Tooltip("The containers or voices this container holds.")]
        public AudioObject[] audioObjects;

        /// <summary>
        /// Should the container repeat itself?
        /// </summary>
        [Tooltip("Should the container repeat itself?")]
        public bool repeat;

        /// <summary>
        /// The types of repetition audio containers can use.
        /// </summary>
        public enum AudioRepeatType { WaitForEnd, TriggerDelay};
        
        /// <summary>
        /// The type of repetition of this audio container.
        /// </summary>
        public AudioRepeatType repeatType;

        /// <summary>
        /// How long should the container wait before repeating.
        /// </summary>
        [Tooltip("How long should the container wait before repeating.")]
        public float containerRepeatDelay;


        [Header("Positioning")]
        public bool holdEmitterPosition = false;
    }
}


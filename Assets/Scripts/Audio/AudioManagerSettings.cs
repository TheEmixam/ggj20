﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Demute
{
    public class AudioManagerSettings : ScriptableObject
    {
        #region Settings


        public AudioContainer masterContainer;


        #endregion


        private static AudioManagerSettings m_Instance = null;
        public static AudioManagerSettings Instance
        {
            get
            {
                if (m_Instance == null)
                {
#if UNITY_EDITOR
                    var name = typeof(AudioManagerSettings).Name;
                    m_Instance = GetOrCreateSettings(name, name);
#else
				m_Instance = CreateInstance<AudioManagerSettings>();
				UnityEngine.Debug.LogWarning("Audio Manager : No specific settings were created. Default initialization settings will be used.");
#endif
                }

                return m_Instance;
            }
        }


#if UNITY_EDITOR
        private static readonly string SettingsRelativeDirectory = System.IO.Path.Combine("Audio", "Resources");
        private static readonly string SettingsAssetDirectory = System.IO.Path.Combine("Assets", SettingsRelativeDirectory);

#if UNITY_EDITOR

        public static AudioManagerSettings GetOrCreateSettings(string className, string fileName)
        {
            var path = AssetFilePath(fileName);
            var asset = UnityEditor.AssetDatabase.LoadAssetAtPath<AudioManagerSettings>(path);
            if (!asset)
            {
                var fullPath = System.IO.Path.Combine(UnityEngine.Application.dataPath, SettingsRelativeDirectory);
                if (!System.IO.Directory.Exists(fullPath))
                    System.IO.Directory.CreateDirectory(fullPath);

                asset = CreateInstance(className) as AudioManagerSettings;
                UnityEditor.AssetDatabase.CreateAsset(asset, path);
            }

            return asset;
        }
#endif


        private static string AssetFilePath(string fileName)
        {
            return System.IO.Path.Combine(SettingsAssetDirectory, fileName + ".asset");
        }

        [UnityEditor.MenuItem("Audio Manager/Audio Settings")]
        private static void AudioSettingsMenuItem()
        {
            UnityEditor.Selection.activeObject = Instance;
        }
#endif
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Demute;

/// <summary>
/// Singleton class for posting events and tracking registered objects.
/// </summary>
public class AudioManager
{
    private static AudioManager _instance;
    public static AudioManager Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = new AudioManager();
            }
            return _instance;
        }
    }

    private Transform _audioPool;
    public Transform audioPool
    {
        get
        {
            if(_audioPool == null)
            {
                _audioPool = new GameObject("Audio Sources Pool").transform;
                _audioPool.SetParent(AudioInitializer.Instance.transform);
            }
            return _audioPool;
        }
    } 

    public List<AudioInstance> pooledAudioInstances = new List<AudioInstance>();
    public List<AudioSource> pooledAudioSources = new List<AudioSource>();
    private Dictionary<int, AudioComponent> registeredObjects = new Dictionary<int, AudioComponent>();

    public enum LoggingLevel { Silent, Debug, Verbose}
    public LoggingLevel loggingLevel;
    private bool _initialized = false;

    public void Init(AudioInitializer audioInitializer)
    {
        loggingLevel = audioInitializer.loggingLevel;
        _initialized = true;
    }

    /// <summary>
    /// Post an audio event on a Game Object.
    /// </summary>
    /// <param name="audioEvent">Audio event to post.</param>
    /// <param name="gameObject">Game Object on which to post the event.</param>
    public static void PostEvent(AudioEvent audioEvent, GameObject gameObject)
    {
        if(audioEvent == null)
        {
            Debug.LogWarningFormat("Audio Manager : tried posting a null audio event on {0}", gameObject.name);
            return;
        }

        if (!Instance._initialized)
        {
            Debug.LogWarning("Audio Manager : the audio manager hasn't been initialized before posting an event. Did you add an audio initializer to your scene?");
            return;
        }

        int audioSourceId = gameObject.GetInstanceID();
        if (!Instance.registeredObjects.ContainsKey(audioSourceId))
        {
            Instance.RegisterGameObject(gameObject);
        }

        PostEvent(audioEvent, Instance.registeredObjects[audioSourceId]);
    }

    public static void PostEvent(AudioEvent audioEvent, AudioComponent audioComponent)
    {
        if (!Instance._initialized)
        {
            Debug.LogWarning("Audio Manager : the audio manager hasn't been initialized before posting an event. Did you add an audio initializer to your scene?");
            return;
        }
        audioComponent.Post(audioEvent);
    }

    /// <summary>
    /// Registers a Game Object in the audio manager list.
    /// </summary>
    /// <param name="gameObject">Game Object to register.</param>
    public void RegisterGameObject(GameObject gameObject)
    {
        AudioComponent audioComponent = gameObject.AddComponent<AudioComponent>();
        registeredObjects.Add(gameObject.GetInstanceID(), audioComponent);
    }


    public AudioInstance PoolAudioInstance()
    {
        if (pooledAudioInstances.Count > 0)
        {
            AudioInstance pooledInstance = pooledAudioInstances[0];
            pooledAudioInstances.Remove(pooledInstance);
            return pooledInstance;
        }
        else
        {
            return new AudioInstance();
        }
    }

#if UNITY_EDITOR
    private AudioComponent _editorAudioComponent;
    public AudioComponent editorAudioComponent
    {
        get
        {
            if(_editorAudioComponent == null)
            {
                _editorAudioComponent = new GameObject("Editor Audio Player").AddComponent<AudioComponent>();
                _editorAudioComponent.gameObject.hideFlags = HideFlags.HideAndDontSave;
                _editorAudioComponent.RegisterEditorAudio();
                
            }

            return _editorAudioComponent;
        }
    }
#endif
}

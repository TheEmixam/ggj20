﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace Demute
{
    [CustomEditor(typeof(AudioVoice))]
    public class AudioVoiceEditor : Editor
    {
        [MenuItem("Assets/Create/Audio/Create Audio Voices from selected Audio Clips")]
        public static void CreateAudioVoicesFromAudioClip()
        {
            foreach(Object obj in Selection.objects)
            {
                if(obj.GetType() == typeof(AudioClip))
                {
                    string assetPath = string.Format("Assets/Audio/AudioVoices/voice_{0}.asset", obj.name);
                    AudioVoice existingVoice = AssetDatabase.LoadAssetAtPath<AudioVoice>(assetPath);
                    if (existingVoice == null)
                    {
                        AudioVoice audioVoiceAsset = CreateInstance<AudioVoice>();
                        audioVoiceAsset.audioClip = obj as AudioClip;
                        Directory.CreateDirectory(Path.GetDirectoryName(assetPath));
                        AssetDatabase.CreateAsset(audioVoiceAsset, assetPath);
                        AssetDatabase.SaveAssets();
                    }
                    else
                    {
                        existingVoice.audioClip = obj as AudioClip;
                    }


                }
            }
        }

        [MenuItem("Assets/Create/Audio/Create Audio Voices from selected Audio Clips", true)]
        public static bool ValidateCreateAudioVoices()
        {
            return Selection.activeObject.GetType() == typeof(AudioClip);
        }

    }
}


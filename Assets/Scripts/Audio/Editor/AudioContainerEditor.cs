﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Demute
{
    [CustomEditor(typeof(AudioContainer))]
    public class AudioContainerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            AudioContainer audioContainer = (AudioContainer)target;
            
            if (GUILayout.Button("Play"))
            {
                AudioEvent editorEvent = ScriptableObject.CreateInstance<AudioEvent>();
                editorEvent.audioActions = new AudioEvent.AudioAction[1] { new AudioEvent.AudioAction { containerAction = AudioEvent.ContainerAction.Play, audioContainer = audioContainer } };

                AudioManager.PostEvent(editorEvent, AudioManager.Instance.editorAudioComponent);
            }

            if (GUILayout.Button("Stop"))
            {
                AudioEvent editorEvent = ScriptableObject.CreateInstance<AudioEvent>();
                editorEvent.audioActions = new AudioEvent.AudioAction[1] { new AudioEvent.AudioAction { containerAction = AudioEvent.ContainerAction.Stop, audioContainer = audioContainer } };

                AudioManager.PostEvent(editorEvent, AudioManager.Instance.editorAudioComponent);

            }


            if (GUILayout.Button("Stop All"))
            {
                AudioEvent editorEvent = ScriptableObject.CreateInstance<AudioEvent>();
                editorEvent.audioActions = new AudioEvent.AudioAction[1] { new AudioEvent.AudioAction { containerAction = AudioEvent.ContainerAction.StopAll, audioContainer = audioContainer } };

                AudioManager.PostEvent(editorEvent, AudioManager.Instance.editorAudioComponent);

            }
        }

    }
}


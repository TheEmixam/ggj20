﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Demute
{
    public class AudioHierarchy : MonoBehaviour
    {
        [SerializeField] List<AudioInspectorTreeElement> m_TreeElements = new List<AudioInspectorTreeElement>();

        internal List<AudioInspectorTreeElement> treeElements
        {
            get { return m_TreeElements; }
            set { m_TreeElements = value; }
        }

        void Awake()
        {


        }

        public static List<AudioInspectorTreeElement> GetData()
        {
            AudioInspectorTreeElement root = new AudioInspectorTreeElement("root", -1, 0, AudioManagerSettings.Instance.masterContainer);
            List<AudioInspectorTreeElement> audioObjects = new List<AudioInspectorTreeElement>();
            audioObjects.Add(root);
            Stack<AudioObjectItem> containersToAdd = new Stack<AudioObjectItem>();

            containersToAdd.Push(new AudioObjectItem(AudioManagerSettings.Instance.masterContainer, 0));
            int index = 1;

            while (containersToAdd.Count > 0)
            {
                index++;
                AudioObjectItem currentItem = containersToAdd.Pop();
                audioObjects.Add(new AudioInspectorTreeElement(currentItem.audioObject.name, currentItem.depth, index, currentItem.audioObject));

                if (currentItem.audioObject.GetType() == typeof(AudioContainer))
                {
                    AudioContainer currentContainer = currentItem.audioObject as AudioContainer;
                    for (int i = 0; i < currentContainer.audioObjects.Length; i++)
                    {
                        containersToAdd.Push(new AudioObjectItem(currentContainer.audioObjects[i], currentItem.depth + 1));
                    }
                }
            }

            return audioObjects;
        }

        public struct AudioObjectItem
        {
            public AudioObject audioObject;
            public int depth;

            public AudioObjectItem(AudioObject audioObject, int depth)
            {
                this.audioObject = audioObject;
                this.depth = depth;
            }
        }
    }
}


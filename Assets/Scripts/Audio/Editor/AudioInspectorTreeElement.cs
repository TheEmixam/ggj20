﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.IMGUI.Controls;
using System;


namespace Demute
{
    [Serializable]
    public class AudioInspectorTreeElement : TreeElement
    {
        public AudioObject audioObject;

        public AudioInspectorTreeElement(string name, int depth, int id, AudioObject audioObject) : base (name, depth, id)
        {
            this.audioObject = audioObject;
        }
    }

    [Serializable]
    public class TreeElement
    {
        [SerializeField] int m_ID;
        [SerializeField] string m_Name;
        [SerializeField] int m_Depth;
        [NonSerialized] TreeElement m_Parent;
        [NonSerialized] List<TreeElement> m_Children;

        public int depth
        {
            get { return m_Depth; }
            set { m_Depth = value; }
        }

        public TreeElement parent
        {
            get { return m_Parent; }
            set { m_Parent = value; }
        }

        public List<TreeElement> children
        {
            get { return m_Children; }
            set { m_Children = value; }
        }

        public bool hasChildren
        {
            get { return children != null && children.Count > 0; }
        }

        public string name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public int id
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        public TreeElement()
        {
        }

        public TreeElement(string name, int depth, int id)
        {
            m_Name = name;
            m_ID = id;
            m_Depth = depth;
        }
    }
}



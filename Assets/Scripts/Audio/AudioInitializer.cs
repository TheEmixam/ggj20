﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Demute
{
    [ExecuteInEditMode]
    public class AudioInitializer : MonoBehaviour
    {
        public AudioManagerSettings settings;

        public AudioManager.LoggingLevel loggingLevel;

        public static AudioInitializer Instance = null;

        private void Awake()
        {
            if (Instance)
            {
                DestroyImmediate(this);
                return;
            }

            Instance = this;

#if UNITY_EDITOR
            if (!UnityEditor.EditorApplication.isPlaying)
                return;
#endif
            //DontDestroyOnLoad(gameObject);
        }

        private void OnEnable()
        {

            settings = AudioManagerSettings.Instance;


        if (Instance == this)
            {
                AudioManager.Instance.Init(this);
            }
            else
            {
                Awake();
            }
        }
    }

}

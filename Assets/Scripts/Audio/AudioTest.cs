﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTest : MonoBehaviour
{

    public Demute.AudioEvent[] audioEvents;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            audioEvents[0].Post(gameObject);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            audioEvents[1].Post(gameObject);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            audioEvents[2].Post(gameObject);
        }
    }
}

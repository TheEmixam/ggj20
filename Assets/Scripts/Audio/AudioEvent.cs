﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Demute
{
    /// <summary>
    /// Scriptable object defining an audio event that executes several audio container actions.
    /// </summary>
    [CreateAssetMenu(fileName = "New Audio Event", menuName = "Audio/Audio Event")]
    public class AudioEvent : ScriptableObject
    {
        /// <summary>
        /// Possible audio container actions.
        /// </summary>
        public enum ContainerAction { Play, Stop, StopAll, PostEvent }

        /// <summary>
        /// Struct defining an audio action.
        /// </summary>
        [System.Serializable]
        public struct AudioAction
        {
            /// <summary>
            /// The container assigned to the audio action (might be null).
            /// </summary>
            public AudioContainer audioContainer;

            /// <summary>
            /// The event assigned to the audio action (might be null).
            /// </summary>
            public AudioEvent audioEvent;

            /// <summary>
            /// The action to execute.
            /// </summary>
            public ContainerAction containerAction;
        }

        /// <summary>
        /// Actions that this audio event should execute.
        /// </summary>
        [Tooltip("Actions that this audio event should execute.")]
        public AudioAction[] audioActions;

        /// <summary>
        /// Posts this event on a GameObject
        /// </summary>
        /// <param name="gameObject">GameObject on which to post the event.</param>
        public void Post(GameObject gameObject)
        {
            AudioManager.PostEvent(this, gameObject);
        }
    }
}


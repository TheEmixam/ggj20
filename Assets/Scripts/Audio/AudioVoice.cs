﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Demute
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "New Audio Voice", menuName = "Audio/Audio Voice")]
    public class AudioVoice : AudioObject
    {
        /// <summary>
        /// Should this audio file loop?
        /// </summary>
        [Tooltip("Should this audio file loop")]
        public bool loop;

        /// <summary>
        /// The audio clip associated to this audio voice.
        /// </summary>
        [Tooltip("The audio clip associated to this audio voice.")]
        public AudioClip audioClip;
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Demute
{
    public class AudioObject : ScriptableObject
    {
        public InheritableParameters parameters = InheritableParameters.DEFAULT;     

        public static InheritableParameters CalculateSummedValues(InheritableParameters inheritedValues, AudioObject current)
        {
            inheritedValues.volume.value *= current.parameters.volume.value;
            inheritedValues.pitch.value += current.parameters.pitch.value;

            return inheritedValues;
        }


    }

    [System.Serializable]
    public struct InheritableParameters
    {
        /// <summary>
        /// The volume of the audio container. Min 0 (silence), Max 1 (default volume).
        /// </summary>
        [Tooltip("The volume of the audio container. Min 0 (silence), Max 1 (default volume)")]
        public RandomizableParameter volume;


        /// <summary>
        /// The pitch of the audio container. Defined in cents (0 = default pitch).
        /// </summary>
        [Tooltip("The pitch of the audio container. Defined in cents (0 = default pitchs.")]
        public RandomizableParameter pitch;

        public InheritableParameters(RandomizableParameter volume, RandomizableParameter pitch)
        {
            this.volume = volume;
            this.pitch = pitch;
        }

        public static InheritableParameters DEFAULT
        {
            get
            {
                return new InheritableParameters(new RandomizableParameter(1), new RandomizableParameter(0, true));
            }
        }
    }


    /// <summary>
    /// Struct defining a randomizable audio parameter.
    /// </summary>
    [System.Serializable]
    public struct RandomizableParameter
    {
        [SerializeField] private float baseValue;
        [SerializeField] private bool random;
        [SerializeField] private float minOffset;
        [SerializeField] private float maxOffset;
        private bool logarithmic;

        public RandomizableParameter(float defaulValue, bool logarithmic = false)
        {
            baseValue = defaulValue;
            random = false;
            minOffset = 0;
            maxOffset = 0;
            this.logarithmic = logarithmic;
        }

        public float value
        {
            get
            {
                if (!random)
                {
                    return baseValue;
                }
                else
                {
                    if (!logarithmic)
                    {
                        return Random.Range(baseValue + minOffset, baseValue + maxOffset);
                    }
                    else
                    {
                        return Random.Range(baseValue + minOffset, baseValue + maxOffset);
                    }
                }
            }
            set
            {
                baseValue = value;
            }
        }
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioAnimationReceiver : MonoBehaviour
{
    public void AudioEvent(AnimationEvent audioEvent)
    {
        if(audioEvent.animatorClipInfo.weight > 0.5)
        {
            AudioManager.PostEvent(audioEvent.objectReferenceParameter as Demute.AudioEvent, gameObject);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Demute
{
    public class AudioLogger : MonoBehaviour
    {
        public static void LogWarningFormat(string format, params object[] args)
        {
            if(AudioManager.Instance.loggingLevel == AudioManager.LoggingLevel.Debug)
            {
                Debug.LogWarningFormat("AudioManager : " + format, args);
            }
        }

        public static void LogDebugFormat(string format, params object[] args)
        {
            if(AudioManager.Instance.loggingLevel == AudioManager.LoggingLevel.Verbose)
            {
                Debug.LogFormat("AudioManager : " + format, args);
            }
        }

    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerGUI : MonoBehaviour
{
	public Player Player;

	public Image MainItem;
	public Image SecondItem;

	private Transform _mainCameraTransform;
	private Transform _myTransform;

	private void Start()
	{
		_mainCameraTransform = Camera.main.transform;
		_myTransform = transform;
	}

	void Update()
    {
        if (!_myTransform || !_mainCameraTransform)
            return;

		_myTransform.LookAt(_myTransform.position + _mainCameraTransform.rotation * Vector3.forward, _mainCameraTransform.rotation * Vector3.up);

		if(Player.pickUp1)
		{
			MainItem.gameObject.SetActive(true);
			MainItem.sprite = Player.pickUp1.Item.VisualItem.Icon;
		}
		else
			MainItem.gameObject.SetActive(false);

		if (Player.pickUp2)
		{
			SecondItem.gameObject.SetActive(true);
			SecondItem.sprite = Player.pickUp2.Item.VisualItem.Icon;
		}
		else
			SecondItem.gameObject.SetActive(false);

	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/EffectItem/GunScriptable")]
public class GunScriptable : EffectItem
{
    public int NbrBulletUse = 5;
    public int NbrBulletThrow = 10;

    public float AngleSpread = 5;
    public GameObject Bullet;

    public float ShootDuration = 1.5f;
    public float ThrowDuration = 0.5f;

    public override void Use(Player playerRef)
    {
        base.Use(playerRef);
        playerRef.StartCoroutine(gunShoot(playerRef));
    }

    public IEnumerator gunShoot(Player playerRef)
    {
        for(int i = 0;i < NbrBulletUse; i++)
        {
            GameObject tempBullet = GameObject.Instantiate(Bullet);
            GunEffect gunEffect = tempBullet.GetComponent<GunEffect>();

            gunEffect.SetLauncher(playerRef);
            gunEffect.GetComponent<Collider>().enabled = true;
            Vector3 startPosition = playerRef.transform.position + playerRef.transform.forward * 0.4f;
            startPosition.y = 0.75f;
            tempBullet.transform.position = startPosition;
            Vector3 euler = playerRef.transform.eulerAngles;
            euler.y = euler.y + Mathf.Lerp(-AngleSpread, AngleSpread, Random.Range(-0.1f, 1.1f));
            tempBullet.transform.eulerAngles = euler;

            yield return new WaitForSeconds(ShootDuration / NbrBulletUse);
        }
    }

    public override void LaunchedUse(Player originPlayer, RaycastHit hitResult, Vector3 launchedPosition, Quaternion launchedRotation)
    {
        base.LaunchedUse(originPlayer, hitResult, launchedPosition, launchedRotation);

        originPlayer.StartCoroutine(gunThrow(launchedPosition));
    }

    public IEnumerator gunThrow(Vector3 launchedPosition)
    {
        for (int i = 0; i < NbrBulletThrow/3; i++)
        {
            float randAngle = Random.Range(0, 120);
            GameObject tempBullet = GameObject.Instantiate(Bullet);
            GunEffect gunEffect = tempBullet.GetComponent<GunEffect>();
            gunEffect.GetComponent<Collider>().enabled = true;

            Vector3 startPosition = launchedPosition;
            startPosition.y = 0.75f;
            Vector3 euler = new Vector3(0, randAngle, 0);
            tempBullet.transform.eulerAngles = euler;
            tempBullet.transform.position = startPosition + tempBullet.transform.forward*0.5f;

            tempBullet = GameObject.Instantiate(Bullet);
            gunEffect = tempBullet.GetComponent<GunEffect>();
            gunEffect.GetComponent<Collider>().enabled = true;

            startPosition = launchedPosition;
            startPosition.y = 0.75f;
            euler = new Vector3(0, randAngle+120, 0);
            tempBullet.transform.eulerAngles = euler;
            tempBullet.transform.position = startPosition + tempBullet.transform.forward * 0.5f;

            tempBullet = GameObject.Instantiate(Bullet);
            gunEffect = tempBullet.GetComponent<GunEffect>();
            gunEffect.GetComponent<Collider>().enabled = true;

            startPosition = launchedPosition;
            startPosition.y = 0.75f;
            euler = new Vector3(0, randAngle+240, 0);
            tempBullet.transform.eulerAngles = euler;
            tempBullet.transform.position = startPosition + tempBullet.transform.forward * 0.5f;

            yield return new WaitForSeconds((ThrowDuration / NbrBulletThrow)/3);
        }
    }
}

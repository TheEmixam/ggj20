﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLifeDisplay : MonoBehaviour
{
    public Image Image;
	public Player Player;

    void Update()
    {
        Image.fillAmount = Player.Life.Value / Player.StartingLife.Value;
    }
}

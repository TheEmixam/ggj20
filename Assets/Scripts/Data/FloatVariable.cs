﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableVariable/FloatVariable")]
public class FloatVariable : ScriptableObject
{
    public float Value;
}

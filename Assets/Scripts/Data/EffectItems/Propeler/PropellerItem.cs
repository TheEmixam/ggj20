﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/EffectItem/Propeller")]
public class PropellerItem : EffectItem
{
	public PropellerEffect EffectPrefab;

	public float Radius;
	public float BumpForce;

	[Header("LaunchedUse")]
	public float UseDuration;
	public float MovementSpeedMultiplier;

	[Header("LaunchedUse")]
	public float Speed;
	public float LifeDuration;
	public Vector3 AttachOffset;

	public override void Use(Player playerRef)
	{
		base.Use(playerRef);

		PropellerEffect effect = Instantiate<PropellerEffect>(EffectPrefab, playerRef.transform.position, playerRef.transform.rotation);
		effect.OriginPlayerRef = playerRef;
	}

	public override void LaunchedUse(Player originPlayer, RaycastHit hitResult, Vector3 launchedPosition, Quaternion launchedRotation)
	{
		base.LaunchedUse(originPlayer, hitResult, launchedPosition, launchedRotation);

		PropellerEffect effect = Instantiate<PropellerEffect>(EffectPrefab, launchedPosition, launchedRotation);
		effect.OriginPlayerRef = originPlayer;

		Player pHit = hitResult.collider ? hitResult.collider.GetComponent<Player>() : null;
		if (pHit)
		{
			pHit.TakeDamage(Damage, originPlayer.transform.forward);
			Vector3 force = Vector3.Scale(pHit.transform.position - launchedPosition, new Vector3(1, 0, 1)).normalized;
			pHit.GetComponent<Rigidbody>().AddForce(force * BumpForce, ForceMode.Impulse);
		}

		Vector3 dir;
		if (hitResult.collider)
			dir = originPlayer.transform.forward + Vector3.Dot(originPlayer.transform.forward, hitResult.normal) * hitResult.normal * -2.0f;
		else
			dir = (originPlayer.transform.position - launchedPosition);

		effect.SetDirection(dir);
		effect.IsLaunched = true;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropellerEffect : MonoBehaviour
{
	public PropellerItem EffectInfo;
	public Transform PropTop;
	public Transform PropBottom;
	public LayerMask HitLayers;
	public float SizeLerpStart;
	public float GrowDuration;
	public float TurnRate;
	[HideInInspector]
	public bool IsLaunched;
	[HideInInspector]
	public Player OriginPlayerRef;

    [Header("Sound")]
    public Demute.AudioEvent throwStopEvent;
    public Demute.AudioEvent useStopEvent;

	private float Radius
	{
		get
		{
			return EffectInfo.Radius * _radiusMultiplier;
		}
	}
	private float _radiusMultiplier = 1.0f;
	private Vector3 _currentDirection;
	private Transform _myTransform;
	private Transform _playerTransform;

	private void Start()
	{
		_myTransform = transform;
		_radiusMultiplier = SizeLerpStart / EffectInfo.Radius;
		_myTransform.localScale = Vector3.one * Radius;
		StartCoroutine(GrowCoroutine());

		if (!IsLaunched && OriginPlayerRef)
			_playerTransform = OriginPlayerRef.transform;

		if (!OriginPlayerRef)
			return;

		if (IsLaunched)
			StartCoroutine(MoveCoroutine());
		else
			StartCoroutine(StayCoroutine());

	}

	IEnumerator GrowCoroutine()
	{
		float startTime = Time.time;
		while (Time.time - startTime < GrowDuration)
		{
			float t = (Time.time - startTime) / GrowDuration;
			_radiusMultiplier = Mathf.Lerp(SizeLerpStart / EffectInfo.Radius, 1.0f, t);
			_myTransform.localScale = Vector3.one * Radius;
			yield return null;
		}
		_radiusMultiplier = 1.0f;
	}

	void Update()
    {
		PropTop.Rotate(Vector3.forward, TurnRate * Time.deltaTime);
		PropBottom.Rotate(Vector3.forward, -TurnRate * Time.deltaTime);

		if(!IsLaunched && _playerTransform)
		{
			_myTransform.position = _playerTransform.position + EffectInfo.AttachOffset;
		}
	}

	IEnumerator StayCoroutine()
	{
		string hash = "Propeller " + gameObject.GetHashCode().ToString();

		if(OriginPlayerRef)
			OriginPlayerRef.SetSpeedModifier(hash, EffectInfo.MovementSpeedMultiplier);

		yield return new WaitForSeconds(EffectInfo.UseDuration);

		if(OriginPlayerRef)
			OriginPlayerRef.RemoveSpeedModifier(hash);

		useStopEvent.Post(OriginPlayerRef.gameObject);
		Destroy(gameObject);
	}

	public void SetDirection(Vector3 dir)
	{
		_currentDirection = dir;
		_currentDirection.y = 0.0f;
		_currentDirection.Normalize();
	}

	IEnumerator MoveCoroutine()
	{
		float startTime = Time.time;
		

		while (Time.time - startTime < EffectInfo.LifeDuration)
		{
			Vector3 offset = _currentDirection * EffectInfo.Speed * Time.deltaTime;

			RaycastHit[] hits = Physics.SphereCastAll(_myTransform.position, Radius, offset, offset.magnitude, HitLayers);
			for(int i = 0; i < hits.Length; i++)
			{
				if(hits[i].collider.gameObject != gameObject)
				{
					PickUp pickup = hits[i].collider.GetComponent<PickUp>();

					if (pickup && !pickup.IsBeingLaunched)
						continue;

					Player p = hits[i].transform.GetComponent<Player>();

					if (p)
					{
						p.TakeDamage(EffectInfo.Damage, _currentDirection);
						Vector3 force = Vector3.Scale(p.transform.position - _myTransform.position, new Vector3(1,0,1)).normalized;
						p.GetComponent<Rigidbody>().AddForce(force * EffectInfo.BumpForce, ForceMode.Impulse);
					}

					Vector3 normal = hits[i].normal;
					normal.y = 0.0f;
					normal.Normalize();
					_currentDirection = _currentDirection + Vector3.Dot(_currentDirection, normal) * normal * -2.0f;
					_currentDirection.y = 0.0f;
					_currentDirection.Normalize();

					offset = _currentDirection * EffectInfo.Speed * Time.deltaTime;

					break;
				}
			}

			_myTransform.position += offset;

			yield return null;
		}

        throwStopEvent.Post(OriginPlayerRef.gameObject);
		Destroy(gameObject);
	}

	private void OnTriggerEnter(Collider other)
	{
		if (IsLaunched)
			return;

		Player p = other.GetComponent<Player>();

		if (p && p != OriginPlayerRef)
		{
			Vector3 force = Vector3.Scale(p.transform.position - _myTransform.position, new Vector3(1, 0, 1)).normalized;

			p.TakeDamage(EffectInfo.Damage, force);
			p.GetComponent<Rigidbody>().AddForce(force * EffectInfo.BumpForce, ForceMode.Impulse);
		}
	}
}

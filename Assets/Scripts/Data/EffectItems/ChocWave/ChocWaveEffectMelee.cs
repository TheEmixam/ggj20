﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChocWaveEffectMelee : ChocWaveEffect
{
    [SerializeField]
    private float _maxAngle = 45.0f;

    private Player _sender;

    private IEnumerator Start()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, _size, _ChocWaveLayer);

        foreach (Collider col in colliders)
        {
            Player player = col.GetComponent<Player>();

            if (player && player != _sender)
            {
                Vector3 forceDirection = player.transform.position - _sender.transform.position;
                forceDirection.y = 0.0f;
                float angle = Vector3.Angle(_sender.transform.forward, forceDirection);

                if(angle < _maxAngle * 0.5f)
                {
                    float magnitude = forceDirection.magnitude;

                    if(magnitude >= _size * 0.5f)
                    {
                        yield return new WaitForSeconds(0.15f);
                    }

                    player.Rigidbody.AddForce(forceDirection.normalized * _force, ForceMode.Impulse);
                    player.TakeDamage(_info.Damage, forceDirection.normalized);
                }
            }
        }

        yield return new WaitForSeconds(1.0f);

        Destroy(gameObject);
    }

    public void SetSender(Player player)
    {
        _sender = player;
    }
}

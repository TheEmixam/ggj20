﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChocWaveEffect : MonoBehaviour
{
    [SerializeField]
    protected ChocWaveItem _info;
    [SerializeField]
    protected float _size = 1.0f;
    [SerializeField]
    protected LayerMask _ChocWaveLayer;
    [SerializeField]
    protected float _force = 20.0f;

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, _size);
    }
#endif
}

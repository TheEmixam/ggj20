﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/EffectItem/ChocWave")]
public class ChocWaveItem : EffectItem
{
	[SerializeField]
	private ChocWaveEffectMelee _chocWaveEffectmelee;
	[SerializeField]
	private ChocWaveEffectDistance _chocWaveEffectDistance;

	public override void Use(Player playerRef)
	{
		base.Use(playerRef);
		ChocWaveEffectMelee effect = Instantiate(_chocWaveEffectmelee, playerRef.transform.position, playerRef.transform.rotation, null);
		effect.SetSender(playerRef);
	}

	public override void LaunchedUse(Player originPlayer, RaycastHit hitResult, Vector3 currentPosition, Quaternion currentRotation)
	{
		base.LaunchedUse(originPlayer, hitResult, currentPosition, currentRotation);
		Instantiate(_chocWaveEffectDistance, currentPosition, currentRotation, null);
	}
}

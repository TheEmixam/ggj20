﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/EffectItem/Laser")]
public class LaserItem : EffectItem
{
	public LaserEffect LaserEffectPrefab;

	public float WarningDuration;
	public float FireDuration;
	public float Width;
	public float LaserPowerValue;
	public AnimationCurve WarningAlphaCurve;
	public AnimationCurve FireAlphaCurve;
	public AnimationCurve FirePowerCurve;
	public Color WarningColor;
	public Color FireColor;

	[Header("LaunchedUse")]
	public float CircleRadius;

	public override void Use(Player playerRef)
	{
		base.Use(playerRef);

		LaserEffect laser = GameObject.Instantiate<LaserEffect>(LaserEffectPrefab, playerRef.transform.position, playerRef.transform.rotation, null);

		laser.PlayerRef = playerRef;
		laser.Fire();
	}

	public override void LaunchedUse(Player originPlayer, RaycastHit hitResult, Vector3 launchedPosition, Quaternion launchedRotation)
	{
		base.LaunchedUse(originPlayer, hitResult, launchedPosition, launchedRotation);

		LaserEffect laser = GameObject.Instantiate<LaserEffect>(LaserEffectPrefab, launchedPosition, launchedRotation, null);

		laser.PlayerRef = hitResult.collider ? hitResult.collider.GetComponent<Player>() : null;
		laser.IsLaunched = true;
		laser.Fire();
	}
}

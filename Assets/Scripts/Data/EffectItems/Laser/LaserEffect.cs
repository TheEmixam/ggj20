﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserEffect : MonoBehaviour
{
	public LaserItem LaserInfo;
	public LayerMask WallLayer;
	public LayerMask HitLayer;
	public int CircleSegments;
	[HideInInspector]
	public Player PlayerRef;
	[HideInInspector]
	public bool IsLaunched;

	private LineRenderer _lineRenderer;
	private Material _laserMat;
	private Transform _playerTransform;

    [Header("Sound")]
    public Demute.AudioEvent laserShootSound;

    void Awake()
    {
		_lineRenderer = GetComponentInChildren<LineRenderer>();
		_laserMat = _lineRenderer.material;
		_lineRenderer.SetPositions(new Vector3[0]);
		_lineRenderer.useWorldSpace = true;
    }

	private void Start()
	{
		if (PlayerRef)
			_playerTransform = PlayerRef.transform;

		if (IsLaunched)
		{
			_lineRenderer.loop = true;
			_lineRenderer.positionCount = CircleSegments;

			float deg = 360.0f / (float)CircleSegments;
			for(int i = 0; i < CircleSegments; i++)
			{
				Vector3 offset = new Vector3(Mathf.Cos(Mathf.Deg2Rad * i * deg), 0.0f, Mathf.Sin(Mathf.Deg2Rad * i * deg));
				offset *= LaserInfo.CircleRadius;
				_lineRenderer.SetPosition(i, transform.position + offset);
			}
		}
	}

	private void Update()
	{
		if (!IsLaunched)
		{
			if (!_playerTransform)
				return;

			_lineRenderer.SetPosition(0, _playerTransform.position);

			RaycastHit hit;
			if (Physics.Raycast(_playerTransform.position, _playerTransform.forward, out hit, 50.0f, WallLayer))
				_lineRenderer.SetPosition(1, hit.point);
			else
				_lineRenderer.SetPosition(1, _playerTransform.position + _playerTransform.forward * 50.0f);
		}
		else if (_playerTransform)
		{
			float deg = 360.0f / (float)CircleSegments;
			for (int i = 0; i < CircleSegments; i++)
			{
				Vector3 offset = new Vector3(Mathf.Cos(Mathf.Deg2Rad * i * deg), 0.0f, Mathf.Sin(Mathf.Deg2Rad * i * deg));
				offset *= LaserInfo.CircleRadius;
				_lineRenderer.SetPosition(i, _playerTransform.position + offset);
			}
		}
	}

	[ContextMenu("Fire")]
	public void Fire()
	{
		StartCoroutine(FireCoroutine());
	}

	IEnumerator FireCoroutine()
	{
		_laserMat.SetFloat("FinalAlpha", 0.0f);
		_laserMat.SetColor("LaserColor", LaserInfo.WarningColor);
		_laserMat.SetFloat("Power", LaserInfo.LaserPowerValue);

		float startTime = Time.time;
		while(Time.time - startTime < LaserInfo.WarningDuration)
		{
			float t = (Time.time - startTime) / LaserInfo.WarningDuration;

			_laserMat.SetFloat("FinalAlpha", LaserInfo.WarningAlphaCurve.Evaluate(t));

			yield return null;
		}
		_laserMat.SetFloat("FinalAlpha", LaserInfo.WarningAlphaCurve.Evaluate(1.0f));

		//Fire laser
		laserShootSound.Post(PlayerRef ? PlayerRef.gameObject : gameObject);
        Vector3[] positions = new Vector3[_lineRenderer.positionCount];
		_lineRenderer.GetPositions(positions);
		List<Player> hitPlayers = new List<Player>();
		for (int i = 0; i < positions.Length - 1; i++)
		{
			Vector3 dir = positions[i + 1] - positions[i];
			RaycastHit[] hits = Physics.SphereCastAll(positions[i], LaserInfo.Width, dir, dir.magnitude, HitLayer);
			
			for(int e = 0; e < hits.Length; e++)
			{
				if(!PlayerRef || hits[e].collider.gameObject != PlayerRef.gameObject)
				{
					Player hitP = hits[e].collider.GetComponent<Player>();
					if(hitP && !hitPlayers.Contains(hitP))
					{
						//Hit player
						hitP.TakeDamage(LaserInfo.Damage, -hits[e].normal);
						hitPlayers.Add(hitP);
					}
				}
			}
		}
		
		_laserMat.SetColor("LaserColor", LaserInfo.FireColor);
		startTime = Time.time;
		while (Time.time - startTime < LaserInfo.FireDuration)
		{
			float t = (Time.time - startTime) / LaserInfo.FireDuration;

			_laserMat.SetFloat("FinalAlpha", LaserInfo.FireAlphaCurve.Evaluate(t));
			_laserMat.SetFloat("Power", LaserInfo.LaserPowerValue * LaserInfo.FirePowerCurve.Evaluate(t));

			yield return null;
		}
		_laserMat.SetFloat("FinalAlpha", LaserInfo.FireAlphaCurve.Evaluate(1.0f));
		_laserMat.SetFloat("Power", LaserInfo.FirePowerCurve.Evaluate(1.0f));

		yield return null;

		_lineRenderer.gameObject.SetActive(false); 

		yield return new WaitForSeconds(2.0f);

		Destroy(gameObject);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlueEffect : MonoBehaviour
{
	public GlueItem EffectInfo;
	public LineRenderer LineRenderer;

	[HideInInspector]
	public Player PlayerOwner;

	private Transform _myTransform;
	private Transform _playerTransform;
	private List<GlueZone> _zones;

	private void Awake()
	{
		_myTransform = transform;
		_zones = new List<GlueZone>();
		LineRenderer.useWorldSpace = true;
		LineRenderer.transform.parent = null;
	}

	IEnumerator Start()
    {
		if (PlayerOwner)
		{
			_playerTransform = PlayerOwner.transform;
			PlayerOwner.SetSpeedModifier("Glue " + gameObject.GetHashCode(), EffectInfo.UseSpeedModifier);
		}

		float startTime = Time.time;
		while (Time.time - startTime < EffectInfo.UseDuration)
		{
			GlueZone zone = Instantiate<GlueZone>(EffectInfo.ZonePrefab, _myTransform.position, _myTransform.rotation);
			zone.Size = EffectInfo.UseSize;
			zone.StayDuration = EffectInfo.UseStayDuration;
			zone.EffectHashCode = gameObject.GetHashCode();
			zone.IgnorePlayer = PlayerOwner;
			//zone.SpriteTransform.GetComponent<SpriteRenderer>().color = new Color(0.0f,0.0f,0.0f,0.2f);
			zone.SpriteTransform.gameObject.SetActive(false);
			_zones.Add(zone);

			yield return new WaitForSeconds(1.0f / EffectInfo.SpawnFrequency);
		}

		if (PlayerOwner)
			PlayerOwner.RemoveSpeedModifier("Glue " + gameObject.GetHashCode());

		yield return new WaitForSeconds(EffectInfo.UseStayDuration + 2.0f);

		Destroy(gameObject);
	}

	private void Update()
	{
		if (PlayerOwner)
			_myTransform.position = _playerTransform.position;

		AnimationCurve curve = new AnimationCurve();

		LineRenderer.positionCount = _zones.Count;
		List<Vector3> points = new List<Vector3>();
		List<float> keys = new List<float>();
		for(int e = 0; e < _zones.Count; e++)
		{
			if (_zones[_zones.Count - e - 1])
			{
				points.Add(_zones[_zones.Count - e - 1].transform.position);
			}
		}
		for (int i = 0; i < points.Count; i++)
		{
			if (_zones[_zones.Count - i - 1])
			{
				float key = i / (points.Count - 1.0f);
				float size = _zones[_zones.Count - i - 1].transform.localScale.x * _zones[_zones.Count - i - 1].Size * 2.0f;
				//size = Mathf.Max(size, 0.2f);
				curve.AddKey(key, size);
				keys.Add(key);
			}
		}
		LineRenderer.positionCount = points.Count;
		LineRenderer.SetPositions(points.ToArray());
		LineRenderer.widthCurve = curve;
	}
}

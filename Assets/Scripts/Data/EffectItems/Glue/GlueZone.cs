﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlueZone : MonoBehaviour
{
	public GlueItem EffectInfo;
	public float HideDuration;
	public AnimationCurve SizeCurve;
	public Transform SpriteTransform;

	[Header("Filled on spawn")]
	public int EffectHashCode;
	public float Size;
	public Player IgnorePlayer;
	public float StayDuration;

	private List<Player> _affectedPlayers;

	private IEnumerator Start()
	{
		_affectedPlayers = new List<Player>();

		GetComponent<SphereCollider>().radius = Size;
		SpriteTransform.localScale = Vector3.one * Size * 2.0f;

		yield return new WaitForSeconds(StayDuration);

		float startTime = Time.time;
		while(Time.time - startTime < HideDuration)
		{
			float t = (Time.time - startTime) / HideDuration;
			transform.localScale = Vector3.one * SizeCurve.Evaluate(t);
			yield return null;
		}

		for (int i = 0; i < _affectedPlayers.Count; i++)
		{
			_affectedPlayers[i].RemoveSpeedModifier("GlueZone " + EffectHashCode.ToString());
		}
		Destroy(gameObject);
	}

	private void Update()
	{
		for(int i = 0; i < _affectedPlayers.Count; i++)
		{
			_affectedPlayers[i].SetSpeedModifier("GlueZone " + EffectHashCode.ToString(), EffectInfo.ZoneSpeedModifier);
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		Player p = other.GetComponent<Player>();

		if(p && p != IgnorePlayer)
		{
			_affectedPlayers.Add(p);
		}
	}

	private void OnTriggerExit(Collider other)
	{
		Player p = other.GetComponent<Player>();

		if (p && p != IgnorePlayer)
		{
			_affectedPlayers.Remove(p);
			p.SetSpeedModifier("GlueZone " + EffectHashCode.ToString(), 1.0f);
		}
	}
}

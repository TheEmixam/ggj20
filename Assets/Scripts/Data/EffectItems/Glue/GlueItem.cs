﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/EffectItem/Glue")]
public class GlueItem : EffectItem
{
	public GlueEffect EffectPrefab;
	public GlueZone ZonePrefab;
	public float ZoneSpeedModifier;

	[Header("Use")]
	public float UseDuration;
	public float UseStayDuration;
	public float UseSize;
	public float SpawnFrequency;
	public float UseSpeedModifier;

	[Header("Launch")]
	public float LaunchSize;
	public float LaunchStayDuration;

	public override void Use(Player playerRef)
	{
		base.Use(playerRef);
		GlueEffect effect = Instantiate<GlueEffect>(EffectPrefab, playerRef.transform.position, playerRef.transform.rotation);
		effect.PlayerOwner = playerRef;
	}

	public override void LaunchedUse(Player originPlayer, RaycastHit hitResult, Vector3 launchedPosition, Quaternion launchedRotation)
	{
		base.LaunchedUse(originPlayer, hitResult, launchedPosition, launchedRotation);

		GlueZone zone = Instantiate<GlueZone>(ZonePrefab, launchedPosition, launchedRotation);
		zone.Size = LaunchSize;
		zone.StayDuration = LaunchStayDuration;
		zone.EffectHashCode = zone.gameObject.GetHashCode();
	}
}

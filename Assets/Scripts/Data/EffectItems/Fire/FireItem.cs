﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/EffectItem/Fire")]
public class FireItem : EffectItem
{
	[SerializeField]
	private FireEffect _fireEffectPrefab;

	public override void Use(Player playerRef)
	{
		base.Use(playerRef);
		playerRef.SetOnFire(Damage);
	}

	public override void LaunchedUse(Player originPlayer, RaycastHit hitResult, Vector3 launchedPosition, Quaternion launchedRotation)
	{
		base.LaunchedUse(originPlayer, hitResult, launchedPosition, launchedRotation);
		Instantiate(_fireEffectPrefab, launchedPosition, launchedRotation, null);
	}
}

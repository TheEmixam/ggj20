﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireEffect : MonoBehaviour
{
    [SerializeField]
    protected FireItem _info;
    [SerializeField]
    protected float _size = 1.0f;
    [SerializeField]
    protected LayerMask _fireLayer;

    private IEnumerator Start()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, _size, _fireLayer);

        foreach (Collider col in colliders)
        {
            Player player = col.GetComponent<Player>();

            if (player)
            {
                Vector3 forceDirection = player.transform.position - transform.position;
                forceDirection.y = 0.0f;
                float magnitude = forceDirection.magnitude;

                if (magnitude >= _size * 0.5f)
                {
                    yield return new WaitForSeconds(0.15f);
                }

                player.SetOnFire(_info.Damage);
            }
        }

        yield return new WaitForSeconds(1.0f);

        Destroy(gameObject);
    }
}

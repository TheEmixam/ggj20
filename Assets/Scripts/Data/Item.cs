﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Item
{
    public VisualItem VisualItem;
    public EffectItem EffectItem;

    public Item(VisualItem visual, EffectItem effect)
    {
        VisualItem = visual;
        EffectItem = effect;
    }
}

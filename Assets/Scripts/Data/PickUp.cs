﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
	public Item Item;
	public Transform VisualTransform;
	public Transform LaunchTargetTranform;

	[Header("Launch settings")]
	public Vector2 MinMaxDistance;
	public Vector2 MinMaxLaunchDuration;
	public LayerMask LaunchMasks;
	public AnimationCurve LaunchHeightCurve;
	public AnimationCurve LaunchLerpCurve;
	public float GroundHeight;
	public LayerMask WallLayer;


	private bool _isAttachedToPlayer;
	public bool IsBeingLaunched
	{
		get
		{
			return _isBeingLaunched;
		}
	}
	private bool _isBeingLaunched;
	private float _colliderRadius;
	private bool _canBePickUp;
	private SphereCollider _myCollider;
	private Player _attachedPlayer;
	private Transform _attachedPlayerTransform;
	private Animator _animator;

	private void Awake()
	{
		_isAttachedToPlayer = false;
		_isBeingLaunched = false;

		_myCollider = GetComponent<SphereCollider>();
		_colliderRadius = _myCollider.radius;

		_animator = GetComponent<Animator>();
		_canBePickUp = true;
	}

	void Start()
    {
		Instantiate(Item.VisualItem.prefab, VisualTransform);
		LaunchTargetTranform.SetParent(null);
	}

	private void Update()
	{
		_animator.SetBool("CanBePickUp", _canBePickUp);

		if(_isAttachedToPlayer && _attachedPlayer.IsStartThrowing())
		{
			LaunchTargetTranform.gameObject.SetActive(true);

			float distance = Mathf.Lerp(MinMaxDistance.x, MinMaxDistance.y, _attachedPlayer.GetThrowPower());

			RaycastHit hit;
			if (Physics.SphereCast(_attachedPlayerTransform.position, _myCollider.radius, _attachedPlayerTransform.forward, out hit, distance, WallLayer))
				distance = hit.distance;

			LaunchTargetTranform.position = _attachedPlayerTransform.position +
				_attachedPlayerTransform.forward *	distance;
			LaunchTargetTranform.position = new Vector3(LaunchTargetTranform.position.x, GroundHeight, LaunchTargetTranform.position.z);
			LaunchTargetTranform.rotation = Quaternion.identity;
		}
		else
			LaunchTargetTranform.gameObject.SetActive(false);
	}

	public void EnablePickUp()
	{
		_myCollider.enabled = true;
		_canBePickUp = true;
	}

	public void DisablePickUp()
	{
		_myCollider.enabled = false;
		_canBePickUp = false;
	}

	private void OnTriggerEnter(Collider other)
	{
		if (_isBeingLaunched)
			return;

		Player p = other.GetComponent<Player>();

		if(p && p.HasEmptySlotItem())
		{
			Equip(p);
		}
	}

	public void Equip(Player playerRef)
	{
		DisablePickUp();
		_isAttachedToPlayer = true;
		_attachedPlayer = playerRef;
		_attachedPlayerTransform = playerRef.transform;
		playerRef.Equip(this);
	}

	[ContextMenu("EditorLaunch")]
	public void EditorLaunch()
	{
		Launch(null, transform.forward, 0.5f);
	}

	public void Use(Player playerRef)
	{
		_isAttachedToPlayer = false;
		_attachedPlayer = null;

		if(Item.EffectItem)
			Item.EffectItem.Use(playerRef);

		DestroyPickUp();
	}

	public void Launch(Player playerRef, Vector3 direction, float force)
	{
		_isAttachedToPlayer = false;
		_attachedPlayer = null;
		transform.rotation = Quaternion.identity;

		StartCoroutine(LaunchCoroutine(playerRef, direction, force));
	}

	IEnumerator LaunchCoroutine(Player playerRef, Vector3 direction, float force)
	{
		float desiredDistance = Mathf.Lerp(MinMaxDistance.x, MinMaxDistance.y, force);
		float desiredDuration = Mathf.Lerp(MinMaxLaunchDuration.x, MinMaxLaunchDuration.y, force);
		Vector3 startPosition = playerRef ? playerRef.transform.position : transform.position;
		Vector3 desiredPosition = startPosition + direction * desiredDistance;
		desiredPosition.y = GroundHeight;

		Debug.Log("Throw item with force " + force + " and duration " + desiredDuration);


		_isBeingLaunched = true;
		_myCollider.enabled = true;

		//yield return new WaitForFixedUpdate();
		//Debug.Break();

		float startTime = Time.time;
		RaycastHit[] hitResults;
		RaycastHit validHit;
		do
		{
			Vector3 lastPosition = transform.position;
			float t = (Time.time - startTime) / desiredDuration;
			float easeT = LaunchLerpCurve.Evaluate(t);
			transform.position = Vector3.Lerp(startPosition, desiredPosition, easeT);
			VisualTransform.localPosition = Vector3.up * LaunchHeightCurve.Evaluate(t);

			Vector3 sphereCastDir = transform.position - lastPosition;
			hitResults = Physics.SphereCastAll(lastPosition, _colliderRadius, sphereCastDir, sphereCastDir.magnitude, LaunchMasks, QueryTriggerInteraction.Collide);

			yield return new WaitForEndOfFrame();
		}
		while (!IsLaunchHitValid(hitResults, playerRef, out validHit) && Time.time - startTime <= desiredDuration);

		Debug.Log("PickUp finished launching");

		if(Item.EffectItem)
			Item.EffectItem.LaunchedUse(playerRef, validHit, transform.position, transform.rotation);

		_myCollider.enabled = false;
		_isBeingLaunched = false;

		yield return null;

		DestroyPickUp();
	}

	private bool IsLaunchHitValid(RaycastHit[] hits, Player playerRef, out RaycastHit validHit)
	{
		for(int i = 0; i < hits.Length; i++)
		{
			if (hits[i].collider.gameObject != gameObject && hits[i].collider.gameObject != playerRef.gameObject)
			{
				validHit = hits[i];
				Debug.Log("Valid hit for " + hits[i].collider.gameObject.name);
				return true;
			}
		}

		validHit = new RaycastHit();
		return false;
	}

	private void DestroyPickUp()
	{
		if(GameSingleton.Instance)
			GameSingleton.Instance.RemovePickup(this);
		Destroy(LaunchTargetTranform.gameObject);
		Destroy(gameObject);
	}

	private void OnDrawGizmos()
	{
		if(Item.VisualItem)
		{
			if(Item.VisualItem.prefab.GetComponentInChildren<MeshFilter>())
				Gizmos.DrawMesh(Item.VisualItem.prefab.GetComponentInChildren<MeshFilter>().sharedMesh, VisualTransform.position, VisualTransform.rotation);
		}
		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere(transform.position, GetComponent<SphereCollider>().radius);
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.blue;
		Gizmos.DrawWireSphere(transform.position + transform.forward * MinMaxDistance.x, 0.25f);
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position + transform.forward * MinMaxDistance.y, 0.25f);
	}
}

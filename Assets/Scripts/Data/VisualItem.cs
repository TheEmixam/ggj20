﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/VisualItem")]
public class VisualItem : ScriptableObject
{
    public Sprite Icon;
    public string DisplayName;
    public GameObject prefab;
	public float LifeRegen = 2.0f;

    [Header("Sound")]
    public Demute.AudioEvent pickupSound;
}

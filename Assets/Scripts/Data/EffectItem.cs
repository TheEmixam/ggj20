﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/EffectItem")]
public class EffectItem : ScriptableObject
{
	public float Damage;

    [Header("Sound")]
    public Demute.AudioEvent useSound;
    public Demute.AudioEvent launchSound;

    public virtual void Use(Player playerRef)
    {
		//Debug.Log("Use effect " + name );
        AudioManager.PostEvent(useSound, playerRef.gameObject);
    }

    public virtual void LaunchedUse(Player originPlayer, RaycastHit hitResult, Vector3 launchedPosition, Quaternion launchedRotation)
    {
		//Debug.Log("Use launched effect " + name);
        AudioManager.PostEvent(launchSound, originPlayer.gameObject);
    }
}

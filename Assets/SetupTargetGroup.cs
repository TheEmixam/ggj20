﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class SetupTargetGroup : MonoBehaviour
{
    public class WeightData
    {
        public float Weight;
        public bool Check;

        public WeightData(float weight, bool check)
        {
            Weight = weight;
            Check = check;
        }
    }

    [SerializeField]
    Player[] _player;

    [SerializeField]
    CinemachineTargetGroup _cinemachineTarget;
    [SerializeField]
    private CinemachineVirtualCamera _vcam;
    [SerializeField]
    private PlayerManager _manager;
    [SerializeField]
    private float _timeLerp = 4.0f;
    [SerializeField]
    private float _lerpDelay = 2.0f;

    private CinemachineTransposer _transposer;
    private Coroutine _offsetCoroutine = null;
    private Dictionary<Player, WeightData> _weightByPlayer = new Dictionary<Player, WeightData>();

    private void Awake()
    {
        _transposer = _vcam.GetCinemachineComponent<CinemachineTransposer>();
    }

    private IEnumerator Start()
    {
        yield return new WaitUntil(() => _manager.GetActivePlayers().Count > 0);
        
        foreach(Player player in _player)
        {
            _weightByPlayer.Add(player, player.gameObject.activeSelf ? new WeightData(1.0f, false) : new WeightData(0.0f, true));
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(_weightByPlayer.Count == 0)
        {
            return;
        }

        foreach(Player player in _weightByPlayer.Keys)
        {
            if
            (
                player.IsDead && 
                _weightByPlayer[player].Weight == 1.0f &&
                !_weightByPlayer[player].Check
            )
            {
                _weightByPlayer[player].Check = true;
                StartCoroutine(WeightCoroutine(player));
            }
        }

        for(int i = 0;i < _player.Length; i++)
        {
            _cinemachineTarget.m_Targets[i].weight = _weightByPlayer[_player[i]].Weight;
        }

        if(GameSingleton.Instance)
        {
            if
            (
                GameSingleton.Instance.GameFinished && 
                GameSingleton.Instance.HaveWinner &&
                _offsetCoroutine == null
            )
            {
                _offsetCoroutine = StartCoroutine(OffsetCoroutine());
            }
        }
    }

    private IEnumerator WeightCoroutine(Player player)
    {
        yield return new WaitForSeconds(_lerpDelay);

        float elapsedTime = 0.0f;

        while (elapsedTime < _timeLerp)
        {
            if(GameSingleton.Instance.GameFinished && !GameSingleton.Instance.OneLeft)
            {
                _weightByPlayer[player].Weight = 1.0f;
                yield break;
            }

            _weightByPlayer[player].Weight = Mathf.Lerp(1.0f, 0.0f, elapsedTime / _timeLerp);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        _weightByPlayer[player].Weight = 0.0f;
    }

    private IEnumerator OffsetCoroutine()
    {
        yield return new WaitForSeconds(_lerpDelay);

        float elapsedTime = 0.0f;
        Vector3 start = _transposer.m_FollowOffset;

        while(elapsedTime < _timeLerp)
        {
            _transposer.m_FollowOffset = Vector3.Lerp(start, new Vector3(0.0f, 5.0f, -15.0f), elapsedTime / _timeLerp);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        _transposer.m_FollowOffset = new Vector3(0.0f, 5.0f, -15.0f);
    }
}

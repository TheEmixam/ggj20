// GENERATED AUTOMATICALLY FROM 'Assets/Data/Action/ControlMap.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @ControlMap : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @ControlMap()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""ControlMap"",
    ""maps"": [
        {
            ""name"": ""ActionMap"",
            ""id"": ""824c2065-df90-4bc7-8c97-b8839084b80b"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""f6b6a295-d80b-488c-85aa-2959af89abbd"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Rotation"",
                    ""type"": ""Value"",
                    ""id"": ""8b4f0f40-8331-46cc-b0e9-a673e01eeee5"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Use"",
                    ""type"": ""Button"",
                    ""id"": ""294578c6-8376-42a8-abc6-efdee7c68ee1"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2)""
                },
                {
                    ""name"": ""Throw"",
                    ""type"": ""Button"",
                    ""id"": ""0d1a4799-c4cd-47f4-b521-bf7990a1f9ed"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2)""
                },
                {
                    ""name"": ""Dash"",
                    ""type"": ""Button"",
                    ""id"": ""bc085ae6-3f44-4c87-9ae6-88be1ba65b91"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2)""
                },
                {
                    ""name"": ""Exchange"",
                    ""type"": ""Button"",
                    ""id"": ""7aaa14f1-f317-453d-a1ba-19ca75f28321"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": ""Press(behavior=2)""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""08c7c148-4723-41f3-a8cc-0ac9c139ac06"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f5b1e8de-5dd2-487a-b39a-90bd252219e5"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Rotation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""eb162006-8d5f-4b02-9b76-c260442fbee5"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Use"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0b7c6e05-43c5-4a78-be36-5d3cda0588a1"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Throw"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""68e1f7d5-3a3b-4ccb-8ec5-3e260e6d4779"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""930ba3d7-b3c4-4d22-ab06-54bf9e84c948"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Exchange"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Gamepad"",
            ""bindingGroup"": ""Gamepad"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // ActionMap
        m_ActionMap = asset.FindActionMap("ActionMap", throwIfNotFound: true);
        m_ActionMap_Move = m_ActionMap.FindAction("Move", throwIfNotFound: true);
        m_ActionMap_Rotation = m_ActionMap.FindAction("Rotation", throwIfNotFound: true);
        m_ActionMap_Use = m_ActionMap.FindAction("Use", throwIfNotFound: true);
        m_ActionMap_Throw = m_ActionMap.FindAction("Throw", throwIfNotFound: true);
        m_ActionMap_Dash = m_ActionMap.FindAction("Dash", throwIfNotFound: true);
        m_ActionMap_Exchange = m_ActionMap.FindAction("Exchange", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // ActionMap
    private readonly InputActionMap m_ActionMap;
    private IActionMapActions m_ActionMapActionsCallbackInterface;
    private readonly InputAction m_ActionMap_Move;
    private readonly InputAction m_ActionMap_Rotation;
    private readonly InputAction m_ActionMap_Use;
    private readonly InputAction m_ActionMap_Throw;
    private readonly InputAction m_ActionMap_Dash;
    private readonly InputAction m_ActionMap_Exchange;
    public struct ActionMapActions
    {
        private @ControlMap m_Wrapper;
        public ActionMapActions(@ControlMap wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_ActionMap_Move;
        public InputAction @Rotation => m_Wrapper.m_ActionMap_Rotation;
        public InputAction @Use => m_Wrapper.m_ActionMap_Use;
        public InputAction @Throw => m_Wrapper.m_ActionMap_Throw;
        public InputAction @Dash => m_Wrapper.m_ActionMap_Dash;
        public InputAction @Exchange => m_Wrapper.m_ActionMap_Exchange;
        public InputActionMap Get() { return m_Wrapper.m_ActionMap; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(ActionMapActions set) { return set.Get(); }
        public void SetCallbacks(IActionMapActions instance)
        {
            if (m_Wrapper.m_ActionMapActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_ActionMapActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_ActionMapActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_ActionMapActionsCallbackInterface.OnMove;
                @Rotation.started -= m_Wrapper.m_ActionMapActionsCallbackInterface.OnRotation;
                @Rotation.performed -= m_Wrapper.m_ActionMapActionsCallbackInterface.OnRotation;
                @Rotation.canceled -= m_Wrapper.m_ActionMapActionsCallbackInterface.OnRotation;
                @Use.started -= m_Wrapper.m_ActionMapActionsCallbackInterface.OnUse;
                @Use.performed -= m_Wrapper.m_ActionMapActionsCallbackInterface.OnUse;
                @Use.canceled -= m_Wrapper.m_ActionMapActionsCallbackInterface.OnUse;
                @Throw.started -= m_Wrapper.m_ActionMapActionsCallbackInterface.OnThrow;
                @Throw.performed -= m_Wrapper.m_ActionMapActionsCallbackInterface.OnThrow;
                @Throw.canceled -= m_Wrapper.m_ActionMapActionsCallbackInterface.OnThrow;
                @Dash.started -= m_Wrapper.m_ActionMapActionsCallbackInterface.OnDash;
                @Dash.performed -= m_Wrapper.m_ActionMapActionsCallbackInterface.OnDash;
                @Dash.canceled -= m_Wrapper.m_ActionMapActionsCallbackInterface.OnDash;
                @Exchange.started -= m_Wrapper.m_ActionMapActionsCallbackInterface.OnExchange;
                @Exchange.performed -= m_Wrapper.m_ActionMapActionsCallbackInterface.OnExchange;
                @Exchange.canceled -= m_Wrapper.m_ActionMapActionsCallbackInterface.OnExchange;
            }
            m_Wrapper.m_ActionMapActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Rotation.started += instance.OnRotation;
                @Rotation.performed += instance.OnRotation;
                @Rotation.canceled += instance.OnRotation;
                @Use.started += instance.OnUse;
                @Use.performed += instance.OnUse;
                @Use.canceled += instance.OnUse;
                @Throw.started += instance.OnThrow;
                @Throw.performed += instance.OnThrow;
                @Throw.canceled += instance.OnThrow;
                @Dash.started += instance.OnDash;
                @Dash.performed += instance.OnDash;
                @Dash.canceled += instance.OnDash;
                @Exchange.started += instance.OnExchange;
                @Exchange.performed += instance.OnExchange;
                @Exchange.canceled += instance.OnExchange;
            }
        }
    }
    public ActionMapActions @ActionMap => new ActionMapActions(this);
    private int m_GamepadSchemeIndex = -1;
    public InputControlScheme GamepadScheme
    {
        get
        {
            if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
            return asset.controlSchemes[m_GamepadSchemeIndex];
        }
    }
    public interface IActionMapActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnRotation(InputAction.CallbackContext context);
        void OnUse(InputAction.CallbackContext context);
        void OnThrow(InputAction.CallbackContext context);
        void OnDash(InputAction.CallbackContext context);
        void OnExchange(InputAction.CallbackContext context);
    }
}

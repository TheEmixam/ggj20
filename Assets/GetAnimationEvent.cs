﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GetAnimationEvent : MonoBehaviour
{

    public UnityEvent OnUse;
    public UnityEvent OnLaunch;
    public UnityEvent OnReadyToLaunch;

    public void Use()
    {
        Debug.LogWarning("Use");
        OnUse.Invoke();
    }

    public void Launch()
    {
        Debug.LogWarning("Launch");
        OnLaunch.Invoke();
    }

    public void ReadyToLaunch()
    {
        Debug.LogWarning("ReadyToLaunch");
        OnReadyToLaunch.Invoke();
    }

}
